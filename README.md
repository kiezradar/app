# KiezRadar - App

Dieses Repository enthält die KiezRadar-App und deren Entwicklungsartefakte.

Derzeit sind wir in der rechtlichen Klärung bzgl. OpenSource, so dass wir erst einmal "nur" das APK der App zum Download anbieten können.

Alles zu Details, Bedienung, Download und zur Installation unter https://kiezradar.gitlab.io/app

- [changelog](changelog.md)


## Gefördert durch

[![Logo "Der Regierende Bürgermeister - Senatskanzlei"](rbmskzl.png "Der Regierende Bürgermeister - Senatskanzlei")](https://www.berlin.de/rbmskzl/)

## Copyright

Copyright 2021-2021 KiezRadar <kiezradar@fokus.fraunhofer.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
