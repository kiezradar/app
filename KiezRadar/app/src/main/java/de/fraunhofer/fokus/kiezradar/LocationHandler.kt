package de.fraunhofer.fokus.kiezradar

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Looper
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import org.osmdroid.util.GeoPoint

object LocationHandler {

    private const val interval: Long = 2000
    private const val fastestInterval: Long = 1000
    private var mLocationRequest: LocationRequest = LocationRequest.create()


    fun checkPermissionForLocation(context: Context, activity: Activity): Boolean {
        return if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            // Show the permission request
            ActivityCompat.requestPermissions( activity , arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                MainActivity.requestPermissionLocation
            )
            false
        }
    }

    fun startLocationUpdates(context: Context) {
        // Create the location request to start receiving updates
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = interval
        mLocationRequest.fastestInterval = fastestInterval

        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        val locationSettingsRequest = builder.build()

        val settingsClient = LocationServices.getSettingsClient(context)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        val mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(
            context
        )
        if (ActivityCompat.checkSelfPermission(context , Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        mFusedLocationProviderClient.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper())
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            locationResult.lastLocation
            onLocationChanged(locationResult.lastLocation)
        }
    }

    fun stopLocationUpdates(context: Context) {
        val mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(
            context)
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
    }

    fun onLocationChanged(location: Location) {
        // set companion variable
        MainActivity.actualLocationCoordinates = GeoPoint (
            location.latitude,
            location.longitude)
    }

    fun getAddressFromGeoCoordinates(lat: Double, lng: Double, context: Context): String {
        val geoCoder = Geocoder(context)
        val list = geoCoder.getFromLocation(lat, lng, 1)
        return list[0].getAddressLine(0)
    }

}