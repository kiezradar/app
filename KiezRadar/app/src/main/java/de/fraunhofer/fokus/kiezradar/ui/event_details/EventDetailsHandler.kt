package de.fraunhofer.fokus.kiezradar.ui.event_details

import androidx.fragment.app.FragmentActivity
import de.fraunhofer.fokus.kiezradar.DataHandler
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.datamodels.EventModel
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler
import kotlin.concurrent.thread

object EventDetailsHandler {
    fun transferAndRequestData(event: EventModel, eventDetailsViewModel: EventDetailsViewModel, activity: FragmentActivity) {

        // use viewModel to transfer the data that was already requested
        eventDetailsViewModel.setUuid(event.uuid)
        eventDetailsViewModel.setTitle(event.title)
        eventDetailsViewModel.setImageUri(event.imageUrl)
        eventDetailsViewModel.setDescription(event.description)

        // reset moreLinks
        eventDetailsViewModel.resetMoreLinkList()

        // request more details
        thread {
            val dataHandler = DataHandler(activity, activity)
            dataHandler.getEvent(event.uuid, eventDetailsViewModel)
        }

        FragmentHandler.loadSubFragment(
            activity.supportFragmentManager,
            EventDetailsFragment(),
            activity.resources.getString(R.string.tag_event_details_fragment),
            ""
        )
    }
}