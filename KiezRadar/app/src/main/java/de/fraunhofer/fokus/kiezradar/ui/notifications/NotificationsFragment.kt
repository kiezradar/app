package de.fraunhofer.fokus.kiezradar.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment

private lateinit var notificationsViewModel: NotificationsViewModel

class NotificationsFragment: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_notifications, container, false)

        (activity as MainActivity).selectNotificationsFragment()

        notificationsViewModel = activity?.run {
            ViewModelProvider(this).get(NotificationsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val fragmentNameTextView = root.findViewById<TextView>(R.id.top_bar_layout_text_view_head_fragment_name)
        fragmentNameTextView.text = resources.getString(R.string.title_notifications_fragment)

        // init filter button + set click listener
        val filterButton: ImageButton = root.findViewById(R.id.button_filter)
        filterButton.setOnClickListener {
            FragmentHandler.loadSubFragment(
                activity?.supportFragmentManager!!,
                FilterFragment(),
                resources.getString(R.string.tag_filter_fragment),
                resources.getString(R.string.tag_notifications_fragment)
            )
        }

        val notificationsButton: ImageButton = root.findViewById(R.id.button_notifications)
        notificationsButton.setOnClickListener {
            (activity as MainActivity).refreshFragment(getString(R.string.tag_notifications_fragment))
        }

        return root
    }
}