package de.fraunhofer.fokus.kiezradar.ui.discover

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomnavigation.BottomNavigationView
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler
import de.fraunhofer.fokus.kiezradar.ui.TilesHandler
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterViewModel
import de.fraunhofer.fokus.kiezradar.ui.notifications.NotificationsFragment

class DiscoverFragment : Fragment() {

    private lateinit var filterViewModel: FilterViewModel
    private lateinit var setFiltersContainer: LinearLayout
    private lateinit var localListSetFilters: ArrayList<String>
    companion object {
        private var idOpenDiscoverSubFragment: Int = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_discover, container, false)

        (activity as MainActivity).selectFragmentInBottomNav(0)

        // initialize value of idOpenDiscoverSubFragment
        if (idOpenDiscoverSubFragment == 0) {
            idOpenDiscoverSubFragment = R.id.discover_nav_current
        }

        filterViewModel = activity?.run {
            ViewModelProvider(this).get(FilterViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val filterButton: ImageButton = root.findViewById(R.id.button_filter)
        filterButton.setOnClickListener {
            FragmentHandler.loadSubFragment(
                activity?.supportFragmentManager!!,
                FilterFragment(),
                resources.getString(R.string.tag_filter_fragment),
                resources.getString(R.string.tag_discover_fragment)
            )
        }
        val notificationsButton: ImageButton = root.findViewById(R.id.button_notifications)
        notificationsButton.setOnClickListener {
            FragmentHandler.loadSubFragment(
                activity?.supportFragmentManager!!,
                NotificationsFragment(),
                resources.getString(R.string.tag_notifications_fragment),
                resources.getString(R.string.tag_discover_fragment)
            )
        }

        setFiltersContainer = root.findViewById(R.id.top_bar_event_filter_container) as LinearLayout

        val topBarNavigation: BottomNavigationView = root.findViewById(R.id.discover_fragment_topNav)
        topBarNavigation.setOnNavigationItemSelectedListener(mOnDiscoverNavigationItemSelectedListener)
        topBarNavigation.selectedItemId = idOpenDiscoverSubFragment

        filterViewModel.radius.observe(viewLifecycleOwner, radiusObserver)

        filterViewModel.periodOfTimeStart.observe(viewLifecycleOwner, periodOfTimeStartObserver)

        filterViewModel.periodOfTimeEnd.observe(viewLifecycleOwner, periodOfTimeEndObserver)

        localListSetFilters = ArrayList()
        filterViewModel.setFilters.observe(viewLifecycleOwner, setFiltersObserver)

        return root
    }

    private val mOnDiscoverNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        idOpenDiscoverSubFragment = item.itemId
        when (item.itemId) {
            R.id.discover_nav_current -> {
                loadDiscoverSubFragment(DiscoverCurrentFragment(), resources.getString(R.string.tag_discover_current_fragment))
                return@OnNavigationItemSelectedListener true
            }
            R.id.discover_nav_near_by -> {
                loadDiscoverSubFragment(DiscoverNearByFragment(), resources.getString(R.string.tag_discover_nearby_fragment))
                return@OnNavigationItemSelectedListener true
            }
            R.id.discover_nav_popular -> {
                loadDiscoverSubFragment(DiscoverPopularFragment(), resources.getString(R.string.tag_discover_popular_fragment))
                return@OnNavigationItemSelectedListener true
            }
            //new discover fragment prepared
//            R.id.discover_nav_private -> {
//                loadDiscoverSubFragment(DiscoverPrivateFragment(), resources.getString(R.string.tag_discover_private_fragment))
//                return@OnNavigationItemSelectedListener true
//            }
        }
        false
    }

    private fun loadDiscoverSubFragment(fragment: Fragment, targetFragmentTag: String) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.container_discover, fragment, targetFragmentTag)
        transaction?.commit()
    }

    private val radiusObserver: Observer<String> = Observer { radius ->
        if ((radius != "") && !filterViewModel.showAll.value!!) {
            context?.let { context ->
                TilesHandler.addFilterTileToContainer(
                    radius,
                    "radius",
                    "" ,
                    radius.hashCode(),
                    context,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val periodOfTimeStartObserver: Observer<String> = Observer { periodStart ->
        if (periodStart != "") {
            context?.let {
                TilesHandler.addFilterTileToContainer(periodStart,
                    "period",
                    "start",
                    periodStart.hashCode(),
                    it,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val periodOfTimeEndObserver: Observer<String> = Observer { periodEnd ->
        if (periodEnd != "") {
            context?.let {
                TilesHandler.addFilterTileToContainer(periodEnd,
                    "period",
                    "end",
                    periodEnd.hashCode(),
                    it,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val setFiltersObserver: Observer<MutableList<String>> = Observer { filters ->
        for (filter in filters) {
            if (!localListSetFilters.contains(filter)) {
                // add selected filter to setFiltersList
                localListSetFilters.add(filter)
                // add view
                context?.let { context ->
                    TilesHandler.addFilterTileToContainer(
                        filter,
                        "other",
                        "",
                        filter.hashCode(),
                        context,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel)
                }
            }
        }
        val filtersToRemove: ArrayList<String> = ArrayList()
        for (filterLocal in localListSetFilters) {
            if (!filters.contains(filterLocal)) {
                filtersToRemove.add(filterLocal)
            }
        }
        localListSetFilters.removeAll(filtersToRemove)
    }

}