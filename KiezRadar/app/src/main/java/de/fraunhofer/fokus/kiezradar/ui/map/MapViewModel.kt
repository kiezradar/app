package de.fraunhofer.fokus.kiezradar.ui.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.osmdroid.util.GeoPoint

class MapViewModel : ViewModel() {

    private val _currentZoom = MutableLiveData<Double>().apply {
        value = 12.0
    }
    val currentZoom: LiveData<Double> = _currentZoom

    fun setCurrentZoom(value: Double) {
        _currentZoom.value = value
    }

    private val _currentMapCenter = MutableLiveData<GeoPoint>().apply {
        value = GeoPoint(52.52437,13.41053)
    }
    val currentMapCenter: LiveData<GeoPoint> = _currentMapCenter

    fun setCurrentMapCenter(value: GeoPoint) {
        _currentMapCenter.value = value
    }

}