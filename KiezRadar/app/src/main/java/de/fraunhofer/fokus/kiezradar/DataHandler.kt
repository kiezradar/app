package de.fraunhofer.fokus.kiezradar

import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import de.fraunhofer.fokus.kiezradar.datamodels.EventModel
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsViewModel
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment
import org.osmdroid.util.GeoPoint
import java.io.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class DataHandler (private val context: Context, private val activity: Activity) {

    private val apiHandler = ApiHandler.getInstance(context)
    private val parser = Parser.getInstance(context)
    private val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.GERMANY)
    private val urlSuffixCurrentPeriod: String = "&amp;periodenddate=gte.${LocalDate.now().format(dateFormatter)}"

    private fun writeStringToInternalStorage(data: String, fileName: String) {
        val fileOutputStream: FileOutputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE)
        fileOutputStream.write(data.toByteArray())
    }

    private fun readStringFromInternalStorage(filename: String): String {
        val fileInputStream: FileInputStream? = context.openFileInput(filename)
        val inputStreamReader = InputStreamReader(fileInputStream)
        val bufferedReader = BufferedReader(inputStreamReader)
        val stringBuilder: StringBuilder = StringBuilder()
        var text: String? = null
        while ({ text = bufferedReader.readLine(); text }() != null) {
            stringBuilder.append(text)
        }

        return stringBuilder.toString()
    }

    private fun updateEventListFilteredSortedByLocation() {
        MainActivity.eventListFilteredSortedByLocation = ArrayList()
        MainActivity.eventListFilteredSortedByLocation.addAll(MainActivity.eventListFilteredSortedByDate)
        MainActivity.eventListFilteredSortedByLocation.removeIf {it.locationType == context.getString(R.string.event_default_property_location_type_no_location)}
        MainActivity.eventListFilteredSortedByLocation.sortBy { MainActivity.actualLocationCoordinates.distanceToAsDouble(it.location[0]) }
    }

    fun reloadDataLastUse() {
        //sections (initialization)
        val fileNameResponseGetAllSections: String = context.getString(R.string.file_name_get_all_sections_response)
        val sectionsApiResponseFromInternalStorage = readStringFromInternalStorage(
            fileNameResponseGetAllSections)
        MainActivity.arrayListSections = parser.parseResponseGetSections(sectionsApiResponseFromInternalStorage)
        //eventTypes (initialization)
        val fileNameResponseGetAllEventTypes: String = context.getString(R.string.file_name_get_all_event_types_response)
        val eventTypesApiResponseFromInternalStorage = readStringFromInternalStorage(
            fileNameResponseGetAllEventTypes)
        MainActivity.arrayListEventTypes = parser.parseResponseGetEventTypes(eventTypesApiResponseFromInternalStorage)
        //districts (initialization)
        val fileNameResponseGetAllDistricts: String = context.getString(R.string.file_name_get_all_districts_response)
        val districtsApiResponseFromInternalStorage = readStringFromInternalStorage(
            fileNameResponseGetAllDistricts)
        MainActivity.arrayListDistricts = parser.parseResponseGetDistricts(districtsApiResponseFromInternalStorage)
    }

    fun getEvent(uuid: String, eventDetailsViewModel: EventDetailsViewModel) {
        fun handleGetEventResponse(apiResponse: String) {
            if (apiResponse != "") {
                val eventModel = parser.parseResponseGetEvent(apiResponse)
                if (eventModel.moreLinks.isNotEmpty()){
                    for (moreLink in eventModel.moreLinks) {
                        eventDetailsViewModel.addMoreLink(moreLink)
                    }
                }
            }
        }
        val eventUrl: String = context.getString(R.string.fullEventUrl) + "&uuid=eq.${uuid}"

        apiHandler.getRequestVolley(eventUrl, ::handleGetEventResponse)
    }

    fun getAllActiveEvents(){
        Debugger.logString("DATAHANDLER", "GETALLEVENTS CALLED")
        val stringBuilder = StringBuilder()
        val eventUrl: String = context.getString(R.string.reducedEventUrl)
        stringBuilder.append(eventUrl)
        stringBuilder.append(urlSuffixCurrentPeriod)
        stringBuilder.append("&order=periodenddate.asc")
        Debugger.logString("DATAHANDLER", "REQUESTED URL (ALL ACTIVE EVENTS): $stringBuilder")
        apiHandler.getRequestVolley(stringBuilder.toString(), ::handleGetAllActiveEventsResponse)
    }

    private fun handleGetAllActiveEventsResponse(apiResponse: String) {
        val fileNameResponseGetAllEvents: String = context.getString(R.string.file_name_get_all_events_response)
        if (apiResponse != "") {
            MainActivity.eventListActivePeriodSortedByDate = apiResponse.let { parser.parseResponseGetEvents(it) }
            Debugger.logString("DATAHANDLER", "GET ACTIVE EVENTS API RESPONSE PARSED")
            writeStringToInternalStorage(apiResponse, fileNameResponseGetAllEvents)
        } else {
            Debugger.logString("DATAHANDLER", "READ EVENTS FROM INTERNAL STORAGE")
            try {
                val apiResponseFromInternalStorage = DataHandler(context, activity).readStringFromInternalStorage(
                    fileNameResponseGetAllEvents)
                MainActivity.eventListActivePeriodSortedByDate = parser.parseResponseGetEvents(apiResponseFromInternalStorage)
            } catch (exc: FileNotFoundException) {
                Debugger.logError("DATAHANDLER","File in internal storage does not exist or is not accessible")
            }
        }

        // initialize filtered event lists
        MainActivity.eventListFilteredSortedByDate = ArrayList()
        MainActivity.eventListFilteredSortedByDate.addAll(MainActivity.eventListActivePeriodSortedByDate)
        updateEventListFilteredSortedByLocation()

        (activity as MainActivity).refreshFragment(context.getString(R.string.tag_discover_fragment))
        activity.refreshFragment(context.getString(R.string.tag_map_fragment))
        activity.refreshFragment(context.getString(R.string.tag_favorites_fragment))
    }

    fun getAllSections(){
        val sectionUrl = context.getString(R.string.sectionUrl)
        apiHandler.getRequestVolley(sectionUrl, ::handleGetAllSectionsResponse)
    }

    private fun handleGetAllSectionsResponse(apiResponse: String) {
        val fileNameResponseGetAllSections: String = context.getString(R.string.file_name_get_all_sections_response)
        if (apiResponse != "") {
            MainActivity.arrayListSections = parser.parseResponseGetSections(apiResponse)
            writeStringToInternalStorage(apiResponse, fileNameResponseGetAllSections)
        } else {
            Debugger.logString("DATAHANDLER", "READ SECTIONS FROM INTERNAL STORAGE")
            try{
                val apiResponseFromInternalStorage = DataHandler(context, activity).readStringFromInternalStorage(
                    fileNameResponseGetAllSections)
                MainActivity.arrayListSections = parser.parseResponseGetSections(apiResponseFromInternalStorage)
            } catch (exc: FileNotFoundException) {
                Debugger.logError("DATAHANDLER","File in internal storage does not exist or is not accessible")
            }
        }
    }

    fun getAllEventTypes(){
        val eventTypeUrl = context.getString(R.string.eventTypeUrl)
        apiHandler.getRequestVolley(eventTypeUrl, ::handleGetAllEventTypesResponse)
    }

    private fun handleGetAllEventTypesResponse(apiResponse: String) {
        val fileNameResponseGetAllEventTypes: String = context.getString(R.string.file_name_get_all_event_types_response)
        if (apiResponse != "") {
            MainActivity.arrayListEventTypes = parser.parseResponseGetEventTypes(apiResponse)
            writeStringToInternalStorage(apiResponse, fileNameResponseGetAllEventTypes)
        } else {
            Debugger.logString("DATAHANDLER", "READ EVENT TYPES FROM INTERNAL STORAGE")
            try {
                val apiResponseFromInternalStorage = DataHandler(context, activity).readStringFromInternalStorage(
                    fileNameResponseGetAllEventTypes)
                MainActivity.arrayListEventTypes = parser.parseResponseGetEventTypes(apiResponseFromInternalStorage)
            } catch (exc: FileNotFoundException) {
                Debugger.logError("DATAHANDLER","File in internal storage does not exist or is not accessible")
            }
        }
    }

    fun getAllDistricts(){
        val districtUrl = context.getString(R.string.districtUrl)
        apiHandler.getRequestVolley(districtUrl, ::handleGetAllDistrictsResponse)
    }

    private fun handleGetAllDistrictsResponse(apiResponse: String) {
        val fileNameResponseGetAllDistricts: String = context.getString(R.string.file_name_get_all_districts_response)
        if (apiResponse != "") {
            MainActivity.arrayListDistricts = parser.parseResponseGetDistricts(apiResponse)
            writeStringToInternalStorage(apiResponse, fileNameResponseGetAllDistricts)
        } else {
            Debugger.logString("DATAHANDLER", "READ DISTRICTS FROM INTERNAL STORAGE")
            try {
                val apiResponseFromInternalStorage = DataHandler(context, activity).readStringFromInternalStorage(
                    fileNameResponseGetAllDistricts)
                MainActivity.arrayListDistricts = parser.parseResponseGetDistricts(apiResponseFromInternalStorage)
            } catch (exc: FileNotFoundException) {
                Debugger.logError("DATAHANDLER","File in internal storage does not exist or is not accessible")
            }
        }
    }

    fun getFilteredEvents(){
        Debugger.logString("DATAHANDLER", "GET FILTERED EVENTS CALLED")
        val stringBuilder = StringBuilder()
        val filteredEventsUrl = generateRequestUrl()
        stringBuilder.append(filteredEventsUrl)
        stringBuilder.append("&order=periodenddate.asc")
        Debugger.logString("DATAHANDLER", "REQUESTED URL (FILTERED EVENTS): $stringBuilder")
        apiHandler.getRequestVolley(stringBuilder.toString(), ::handleGetFilteredEventsResponse)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun generateRequestUrl(): String {
        fun generatePeriodUrl (periodStart: LocalDate, periodEnd: LocalDate) : String {
            val stringBuilder = StringBuilder()
            val periodStartString = periodStart.format(dateFormatter)
            val periodEndString = periodEnd.format(dateFormatter)
            if ((periodStart != LocalDate.of(0,1, 1)) && (periodEnd != LocalDate.of(0,1, 1))) {
                stringBuilder.append("&and=(periodenddate.gte.$periodStartString,periodenddate.lte.$periodEndString)")
            } else if ((periodStart != LocalDate.of(0,1, 1)) && (periodEnd == LocalDate.of(0,1, 1))) {
                stringBuilder.append("&periodenddate=gte.$periodStartString")
            } else if ((periodStart == LocalDate.of(0,1, 1)) && (periodEnd != LocalDate.of(0,1, 1))) {
                stringBuilder.append("&periodenddate=lte.$periodEndString")
            } else {
                Debugger.logWarning("DATAHANDLER", "Case should not be reached")
            }
            return stringBuilder.toString()
        }
        fun generateFilterUrl (setFilters: ArrayList<String>) : String {
            val stringBuilder = StringBuilder()
            val intersectionSetFiltersEventTypes = setFilters.intersect(MainActivity.arrayListEventTypes.map { it.title })
            val intersectionSetFilterDistricts = setFilters.intersect(MainActivity.arrayListDistricts.map {it.title})
            if(intersectionSetFilterDistricts.isNotEmpty() && intersectionSetFiltersEventTypes.isNotEmpty()) {
                stringBuilder.append("&and=(or(")
            } else if (intersectionSetFilterDistricts.isNotEmpty() or intersectionSetFiltersEventTypes.isNotEmpty()) {
                stringBuilder.append("&or=(")
            }

            val districtUuids = ArrayList<String>()
            for (setFilter in setFilters) {
                if (MainActivity.arrayListDistricts.map { it.title }.contains(setFilter)) {
                    districtUuids.add(MainActivity.arrayListDistricts.filter { it.title == setFilter }[0].uuid)
                }
            }
            for (districtUuid in districtUuids) {
                stringBuilder.append("district.eq.$districtUuid")
                if ((districtUuids.size > 1) && (districtUuids[districtUuids.size-1] != districtUuid)) {
                    stringBuilder.append(",")
                }
            }

            if(intersectionSetFilterDistricts.isNotEmpty() && intersectionSetFiltersEventTypes.isNotEmpty()) {
                stringBuilder.append("),")
            }

            val eventTypeUuids = ArrayList<String>()
            for (setFilter in setFilters) {
                if (MainActivity.arrayListEventTypes.map { it.title }.contains(setFilter)) {
                    eventTypeUuids.add(MainActivity.arrayListEventTypes.filter { it.title == setFilter }[0].uuid)
                }
            }
            for (eventTypeUuid in eventTypeUuids) {
                stringBuilder.append("eventtype.eq.$eventTypeUuid")
                if ((eventTypeUuids.size > 1) && (eventTypeUuids[eventTypeUuids.size-1] != eventTypeUuid)) {
                    stringBuilder.append(",")
                }
            }

            if (intersectionSetFilterDistricts.isNotEmpty() or intersectionSetFiltersEventTypes.isNotEmpty()) {
                stringBuilder.append(")")
            }

            return stringBuilder.toString()
        }
        fun generateLocationUrl(radius: Int, location: GeoPoint) : String {
            val stringBuilder = StringBuilder()
            stringBuilder.append("&")
            if ((radius != 0)) {
                stringBuilder.append("distance=${radius*1000}")
                stringBuilder.append("&lat=${location.latitude}")
                stringBuilder.append("&lon=${location.longitude}")
            }
            return stringBuilder.toString()
        }


        when {
            allFiltersSet() -> {
                val stringBuilder = StringBuilder()
                val eventUrl: String = context.getString(R.string.reducedEventUrlRpc)
                stringBuilder.append(eventUrl)
                stringBuilder.append(generateFilterUrl(FilterFragment.setFiltersList))
                stringBuilder.append(generateLocationUrl(FilterFragment.setRadius, FilterFragment.setAddress))
                stringBuilder.append(generatePeriodUrl(FilterFragment.setPeriodStart, FilterFragment.setPeriodEnd))

                return stringBuilder.toString()

            }
            setFilterListAndRadiusFilterSet() -> {
                val stringBuilder = StringBuilder()
                val eventUrl: String = context.getString(R.string.reducedEventUrlRpc)
                stringBuilder.append(eventUrl)
                stringBuilder.append(generateFilterUrl(FilterFragment.setFiltersList))
                stringBuilder.append(generateLocationUrl(FilterFragment.setRadius, FilterFragment.setAddress))
                stringBuilder.append(urlSuffixCurrentPeriod)

                return stringBuilder.toString()

            }
            radiusFilterAndPeriodFilterSet() -> {
                val stringBuilder = StringBuilder()
                val eventUrl: String = context.getString(R.string.reducedEventUrlRpc)
                stringBuilder.append(eventUrl)
                stringBuilder.append(generateLocationUrl(FilterFragment.setRadius, FilterFragment.setAddress))
                stringBuilder.append(generatePeriodUrl(FilterFragment.setPeriodStart, FilterFragment.setPeriodEnd))

                return stringBuilder.toString()

            }
            setFilterListAndPeriodFilterSet() -> {
                val stringBuilder = StringBuilder()
                val eventUrl: String = context.getString(R.string.reducedEventUrl)
                stringBuilder.append(eventUrl)
                stringBuilder.append(generateFilterUrl(FilterFragment.setFiltersList))
                stringBuilder.append(generatePeriodUrl(FilterFragment.setPeriodStart, FilterFragment.setPeriodEnd))

                return stringBuilder.toString()

            }
            setFilterListSet() -> {
                val stringBuilder = StringBuilder()
                val eventUrl: String = context.getString(R.string.reducedEventUrl)
                stringBuilder.append(eventUrl)
                stringBuilder.append(generateFilterUrl(FilterFragment.setFiltersList))
                stringBuilder.append(urlSuffixCurrentPeriod)

                return stringBuilder.toString()

            }
            radiusFilterSet() -> {
                val stringBuilder = StringBuilder()
                val baseUrl: String = context.getString(R.string.baseUrl)
                stringBuilder.append(baseUrl)
                stringBuilder.append("/rpc/filter_distance_ev?select=description,periodenddate,periodendtime,periodstartdate,periodstarttime,source,title,uuid,eventtype(*),section!nmeventsection(*),location(*),district(*),image(*),apilinks!apilinks(*,link!nmlinkslink(*)),morelinks!morelinks(*,link!nmlinkslink(*)),files:filelink!nmeventfilelink(*)")
                stringBuilder.append(generateLocationUrl(FilterFragment.setRadius, FilterFragment.setAddress))
                stringBuilder.append(urlSuffixCurrentPeriod)

                return stringBuilder.toString()

            }
            periodFilterSet() -> {
                val stringBuilder = StringBuilder()
                val eventUrl: String = context.getString(R.string.reducedEventUrl)
                stringBuilder.append(eventUrl)
                stringBuilder.append(generatePeriodUrl(FilterFragment.setPeriodStart, FilterFragment.setPeriodEnd))

                return stringBuilder.toString()

            }
            noFilterSet() -> {
                val eventUrl: String = context.getString(R.string.reducedEventUrl)

                return eventUrl.plus(urlSuffixCurrentPeriod)

            }
            else -> {
                Debugger.logWarning("DATAHANDLER", "CASE SHOULD NOT BE REACHED")
                return ""
            }
        }

    }

    private fun handleGetFilteredEventsResponse(apiResponse: String) {
        if (apiResponse != "") {
            MainActivity.eventListFilteredSortedByDate = apiResponse.let { parser.parseResponseGetEvents(it) }

            applySectionFilter()

            updateEventListFilteredSortedByLocation()

            (activity as MainActivity).refreshFragment(context.getString(R.string.tag_discover_fragment))
            activity.refreshFragment(context.getString(R.string.tag_map_fragment))
            activity.refreshFragment(context.getString(R.string.tag_favorites_fragment))
        }
    }

    private fun applySectionFilter(){
        val setSectionFilter = ArrayList<String>()
        for (setFilter in FilterFragment.setFiltersList) {
            if (MainActivity.arrayListSections.map { it.title }.contains(setFilter)) {
                setSectionFilter.add(setFilter)
            }
        }
        if (setSectionFilter.isNotEmpty()) {
            // section filtering just need to applied to default filtered list (default list: MainActivity.eventListFilteredSortedByDate)
            val eventLists = listOf(MainActivity.eventListFilteredSortedByDate)
            for (eventList in eventLists) {
                val eventsToDelete = ArrayList<EventModel>()
                for (event in eventList) {
                    if (event.sections.intersect(setSectionFilter).isEmpty()) {
                        eventsToDelete.add(event)
                    }
                }
                eventList.removeAll(eventsToDelete)
            }
        }
    }

    fun storeFavorites(){
        val data = StringBuilder()
        data.append("[")
        for (favorite in MainActivity.eventListFavorites) {
            data.append("{\"uuid\":")
            data.append("\"")
            data.append(favorite.uuid)
            data.append("\"")
            data.append("}")

            if (MainActivity.eventListFavorites.indexOf(favorite) < (MainActivity.eventListFavorites.size -1)) {
                data.append(",")
            }
        }
        data.append("]")
        writeStringToInternalStorage(data.toString(), context.getString(R.string.file_name_favorites))
    }

    fun restoreFavorites(){
        val string = readStringFromInternalStorage(context.getString(R.string.file_name_favorites))
        Debugger.logString("DATAHANDLER", "FAVS String from Storage: $string")
    }

    private fun allFiltersSet(): Boolean {
        return (FilterFragment.setFiltersList.isNotEmpty()
                && (FilterFragment.setRadius > 0)
                && ((FilterFragment.setPeriodStart != LocalDate.of(0,1, 1)) || (FilterFragment.setPeriodEnd != LocalDate.of(0,1, 1))))
    }

    private fun setFilterListAndRadiusFilterSet(): Boolean {
        return (FilterFragment.setFiltersList.isNotEmpty()
                && (FilterFragment.setRadius > 0)
                && ((FilterFragment.setPeriodStart == LocalDate.of(0,1, 1)) || (FilterFragment.setPeriodEnd == LocalDate.of(0,1, 1))))
    }

    private fun radiusFilterAndPeriodFilterSet(): Boolean {
        return (FilterFragment.setFiltersList.isEmpty()
                && (FilterFragment.setRadius > 0)
                && ((FilterFragment.setPeriodStart != LocalDate.of(0,1, 1)) || (FilterFragment.setPeriodEnd != LocalDate.of(0,1, 1))))
    }

    private fun setFilterListAndPeriodFilterSet(): Boolean {
        return (FilterFragment.setFiltersList.isNotEmpty()
                && (FilterFragment.setRadius == 0)
                && ((FilterFragment.setPeriodStart != LocalDate.of(0,1, 1)) || (FilterFragment.setPeriodEnd != LocalDate.of(0,1, 1))))
    }

    private fun setFilterListSet(): Boolean {
        return (FilterFragment.setFiltersList.isNotEmpty()
                && FilterFragment.setRadius == 0
                && FilterFragment.setPeriodStart == LocalDate.of(0,1, 1) && FilterFragment.setPeriodEnd == LocalDate.of(0,1, 1))
    }

    private fun radiusFilterSet(): Boolean {
        return (FilterFragment.setFiltersList.isEmpty()
                && (FilterFragment.setRadius > 0)
                && ((FilterFragment.setPeriodStart == LocalDate.of(0,1, 1)) || (FilterFragment.setPeriodEnd == LocalDate.of(0,1, 1))))
    }

    private fun periodFilterSet(): Boolean {
        return (FilterFragment.setFiltersList.isEmpty()
                && (FilterFragment.setRadius == 0)
                && ((FilterFragment.setPeriodStart != LocalDate.of(0,1, 1)) || (FilterFragment.setPeriodEnd != LocalDate.of(0,1, 1))))
    }

    private fun noFilterSet(): Boolean {
        return (FilterFragment.setFiltersList.isEmpty()
                && (FilterFragment.setRadius == 0)
                && ((FilterFragment.setPeriodStart == LocalDate.of(0,1, 1)) || (FilterFragment.setPeriodEnd == LocalDate.of(0,1, 1))))
    }
}