package de.fraunhofer.fokus.kiezradar

import android.util.Log

const val debug : Boolean = true
const val generateLogString: Boolean = false
var logString = StringBuilder()


open class Debugger {

    companion object {

        fun logString(tag: String, string: String) {
            if (debug) {
                Log.d(tag, string)
            }
            if (generateLogString) {
                logString.append("$tag: $string")
                logString.append(System.getProperty("line.separator"))
            }
        }

        fun logWarning(tag: String, string: String) {
            Log.w(tag, string)
        }

        fun logError(tag: String, string: String) {
            Log.e(tag, string)
        }

    }

}