package de.fraunhofer.fokus.kiezradar.ui.filter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FilterViewModel : ViewModel() {

    private val _setFiltersList = MutableLiveData<MutableList<String>>().apply {
    }
    val setFilters: LiveData<MutableList<String>> = _setFiltersList

    private val filtersList = ArrayList<String>()

    fun addFilter(filterValue: String) {
        if (filterValue !in filtersList) {
            filtersList.add(filterValue)
            _setFiltersList.value = filtersList
        }
    }

    fun removeFilter(value: String) {
        if (value in filtersList) {
            filtersList.removeAt(filtersList.indexOf(value))
            _setFiltersList.value = filtersList
        }
    }

    private val _radius = MutableLiveData<String>().apply {
    }
    val radius: LiveData<String> = _radius

    fun setRadius(value: String) {
        _radius.value = value
    }

    private val _showAll = MutableLiveData<Boolean>().apply {
        value = true
    }
    val showAll: LiveData<Boolean> = _showAll

    fun setShowAll(value: Boolean) {
        _showAll.value = value
    }

    private val _address = MutableLiveData<String>().apply {

    }
    val address: LiveData<String> = _address

    fun setAddress(value: String) {
        _address.value = value
    }

    private val _addressIsSelected = MutableLiveData<Boolean>().apply {
        value = false

    }
    val addressIsSelected: LiveData<Boolean> = _addressIsSelected

    fun setAddressIsSelected(value: Boolean) {
        _addressIsSelected.value = value
    }

    private val _periodOfTimeSelection = MutableLiveData<String>().apply {
        value = ""
    }
    val periodOfTimeSelection: LiveData<String> = _periodOfTimeSelection

    fun setPeriodOfTimeSelection(value: String) {
        _periodOfTimeSelection.value = value
    }

    private val _periodOfTimeStart = MutableLiveData<String>().apply {

    }
    val periodOfTimeStart: LiveData<String> = _periodOfTimeStart

    fun setPeriodOfTimeStart(value: String) {
        _periodOfTimeStart.value = value
    }

    private val _periodOfTimeEnd = MutableLiveData<String>().apply {

    }
    val periodOfTimeEnd: LiveData<String> = _periodOfTimeEnd

    fun setPeriodOfTimeEnd(value: String) {
        _periodOfTimeEnd.value = value
    }
}