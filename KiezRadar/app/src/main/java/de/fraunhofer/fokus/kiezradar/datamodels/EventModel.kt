package de.fraunhofer.fokus.kiezradar.datamodels

import org.osmdroid.api.IGeoPoint
import java.time.LocalDateTime
import kotlin.collections.ArrayList

class EventModel {
    lateinit var uuid: String
    lateinit var title: String
    lateinit var eventType: String
    var sections: ArrayList<String> = ArrayList()
    lateinit var periodStart: LocalDateTime
    lateinit var periodEnd: LocalDateTime
    lateinit var locationType: String
    lateinit var locationDistrict: String
    var location: ArrayList<IGeoPoint> = ArrayList()
    lateinit var locationTitle: String
    lateinit var locationDescription: String
    lateinit var description: String
    lateinit var imageUrl: String
    var moreLinks: ArrayList<MoreLink> = ArrayList()
}

class MoreLink {
    lateinit var uuid: String
    lateinit var title: String
    lateinit var url: String
}