package de.fraunhofer.fokus.kiezradar.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import de.fraunhofer.fokus.kiezradar.EventListViewAdapter
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsViewModel
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler.loadSubFragment
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsHandler
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment
import de.fraunhofer.fokus.kiezradar.ui.notifications.NotificationsFragment


class FavoritesFragment : Fragment() {

    private lateinit var favoritesViewModel: FavoritesViewModel
    private lateinit var eventDetailsViewModel: EventDetailsViewModel
    private lateinit var listViewFavorites: ListView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_favorites, container, false)

        (activity as MainActivity).selectFragmentInBottomNav(2)

        val fragmentTextViewName = root.findViewById<TextView>(R.id.top_bar_layout_text_view_head_fragment_name)
        fragmentTextViewName.text = resources.getString(R.string.title_favorites_fragment)

        favoritesViewModel = activity?.run {
            ViewModelProvider(this).get(FavoritesViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        eventDetailsViewModel = activity?.run {
            ViewModelProvider(this).get(EventDetailsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val swipeRefreshLayout = root.findViewById<SwipeRefreshLayout>(R.id.swipe_refresh_favorites)
        swipeRefreshLayout.setOnRefreshListener {
            context?.getString(R.string.tag_favorites_fragment)?.let { fragmentTag ->
                (activity as MainActivity).refreshFragment(
                    fragmentTag
                )
            }
        }

        val filterButton: ImageButton = root.findViewById(R.id.button_filter)
        filterButton.setOnClickListener {
            loadSubFragment(activity?.supportFragmentManager!!,
                            FilterFragment(),
                            resources.getString(R.string.tag_filter_fragment),
                            resources.getString(R.string.tag_favorites_fragment))
        }
        val notificationsButton: ImageButton = root.findViewById(R.id.button_notifications)
        notificationsButton.setOnClickListener {
            loadSubFragment(activity?.supportFragmentManager!!,
                            NotificationsFragment(),
                            resources.getString(R.string.tag_notifications_fragment),
                            resources.getString(R.string.tag_favorites_fragment))
        }

        listViewFavorites = root.findViewById(R.id.eventListViewFavorites) as ListView
        val adapter = EventListViewAdapter(requireContext(), MainActivity.eventListFavorites)
        listViewFavorites.adapter = adapter

        listViewFavorites.setOnItemClickListener { _, _, position, _ ->
            // use companion object to get event
            val selectedEventInEventDetails = MainActivity.eventListFavorites[position]

            EventDetailsHandler.transferAndRequestData(
                selectedEventInEventDetails,
                eventDetailsViewModel,
                requireActivity())
        }

        return root
    }



}