package de.fraunhofer.fokus.kiezradar.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler.loadSubFragment
import de.fraunhofer.fokus.kiezradar.ui.TilesHandler
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterViewModel
import de.fraunhofer.fokus.kiezradar.ui.notifications.NotificationsFragment

class SearchFragment : Fragment() {

    private lateinit var searchViewModel: SearchViewModel
    private lateinit var filterViewModel: FilterViewModel
    private lateinit var setFiltersContainer: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_search, container, false)

        (activity as MainActivity).selectFragmentInBottomNav(3)

        searchViewModel = activity?.run {
            ViewModelProvider(this).get(SearchViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        filterViewModel = activity?.run {
            ViewModelProvider(this).get(FilterViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        // init filter/notification button + set click listener
        val filterButton: ImageButton = root.findViewById(R.id.button_filter)
        filterButton.setOnClickListener {
            loadSubFragment(activity?.supportFragmentManager!!,
                            FilterFragment(),
                            resources.getString(R.string.tag_filter_fragment),
                            resources.getString(R.string.tag_search_fragment))
        }
        val notificationsButton: ImageButton = root.findViewById(R.id.button_notifications)
        notificationsButton.setOnClickListener {
            loadSubFragment(activity?.supportFragmentManager!!,
                            NotificationsFragment(),
                            resources.getString(R.string.tag_notifications_fragment),
                            resources.getString(R.string.tag_search_fragment))
        }

        setFiltersContainer = root.findViewById(R.id.top_bar_event_filter_container) as LinearLayout

        filterViewModel.radius.observe(viewLifecycleOwner, { radius ->
            if ((radius != "") && !filterViewModel.showAll.value!!) {
                context?.let { context ->
                    TilesHandler.addFilterTileToContainer(
                        radius,
                        "radius",
                        "" ,
                        radius.hashCode(),
                        context,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel
                    )
                }
            }
        })

        filterViewModel.periodOfTimeStart.observe(viewLifecycleOwner, { periodStart ->
            if (periodStart != "") {
                context?.let {
                    TilesHandler.addFilterTileToContainer(periodStart,
                        "period",
                        "start",
                        periodStart.hashCode(),
                        it,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel
                    )
                }
            }
        })

        filterViewModel.periodOfTimeEnd.observe(viewLifecycleOwner, { periodEnd ->
            if (periodEnd != "") {
                context?.let {
                    TilesHandler.addFilterTileToContainer(periodEnd,
                        "period",
                        "end",
                        periodEnd.hashCode(),
                        it,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel
                    )
                }
            }
        })

        val tempListSetFilters: ArrayList<String> = ArrayList()
        filterViewModel.setFilters.observe(viewLifecycleOwner, { filters ->
            for (filter in filters) {
                if (!tempListSetFilters.contains(filter)) {
                    // add selected filter to setFiltersList
                    tempListSetFilters.add(filter)
                    // add view
                    context?.let { context ->
                        TilesHandler.addFilterTileToContainer(
                            filter,
                            "other",
                            "",
                            filter.hashCode(),
                            context,
                            requireActivity(),
                            setFiltersContainer,
                            filterViewModel)
                    }
                }
            }
            val filtersToRemove: ArrayList<String> = ArrayList()
            for (filterLocal in tempListSetFilters) {
                if (!filters.contains(filterLocal)) {
                    filtersToRemove.add(filterLocal)
                }
            }
            tempListSetFilters.removeAll(filtersToRemove)
        })

        return root
    }
}