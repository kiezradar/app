package de.fraunhofer.fokus.kiezradar

import android.content.Context
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlin.reflect.KFunction1

class ApiHandler private constructor(context: Context){

    companion object : SingletonHolder<ApiHandler, Context>(::ApiHandler)

    private val queue = Volley.newRequestQueue(context)

    fun getRequestVolley(url: String, requestCallback: KFunction1<String, Unit>) {
        val stringRequest = StringRequest(Request.Method.GET, url,
            { response ->
                requestCallback(response)
            },
            {   volleyError ->
                Debugger.logWarning("API HANDLER", "GET REQ VOLLEY | RESPONSE ERROR | STACKTRACE : $volleyError")
            })
        // Add the request to the RequestQueue
        queue.add(stringRequest)
    }

}
