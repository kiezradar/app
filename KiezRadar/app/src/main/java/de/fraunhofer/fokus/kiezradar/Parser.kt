package de.fraunhofer.fokus.kiezradar

import android.content.Context
import de.fraunhofer.fokus.kiezradar.datamodels.*
import org.json.JSONArray
import org.json.JSONObject
import org.osmdroid.util.GeoPoint
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import kotlin.collections.ArrayList

class Parser private constructor(private val context: Context) {

    companion object : SingletonHolder<Parser, Context>(::Parser)

    private fun parseEventReducedInfo(jsonObjectDetail: JSONObject): EventModel {
        val eventModel = EventModel()
        eventModel.uuid = jsonObjectDetail.getString("uuid")
        eventModel.title = jsonObjectDetail.getString("title")
        eventModel.eventType = jsonObjectDetail.getJSONObject("eventtype").getString("title")

        for (sectionPosition in 0 until jsonObjectDetail.getJSONArray("section").length()) {
            val jsonObjectSection: JSONObject =jsonObjectDetail.getJSONArray("section").getJSONObject(sectionPosition)
            eventModel.sections.add(jsonObjectSection.getString("title"))
        }

        if (jsonObjectDetail.getString("description") != "null") {
            eventModel.description = jsonObjectDetail.getString("description")
        } else {
            eventModel.description = context.getString(R.string.event_default_property_description)
        }

        if (jsonObjectDetail.getJSONObject("image").getString("url") != "null") {
            eventModel.imageUrl = jsonObjectDetail.getJSONObject("image").getString("url")
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O &&
            jsonObjectDetail.getString("periodstartdate") != "null") {

            val startDate = LocalDate.parse(jsonObjectDetail.getString("periodstartdate"), DateTimeFormatter.ISO_LOCAL_DATE)

            if (jsonObjectDetail.getString("periodstarttime") != "null") {
                val startTime = LocalTime.parse(jsonObjectDetail.getString("periodstarttime"))
                eventModel.periodStart = startDate.atTime(startTime)
            } else {
                eventModel.periodStart = startDate.atStartOfDay()
            }

            if (jsonObjectDetail.getString("periodenddate") != "null") {
                val endDate = LocalDate.parse(jsonObjectDetail.getString("periodenddate"), DateTimeFormatter.ISO_LOCAL_DATE)

                if (jsonObjectDetail.getString("periodendtime") != "null") {
                    val endTime = LocalTime.parse(jsonObjectDetail.getString("periodendtime"))
                    eventModel.periodEnd = endDate.atTime(endTime)
                } else {
                    eventModel.periodEnd = endDate.atTime(11, 59)
                }

            } else {
                eventModel.periodEnd = eventModel.periodStart
            }
        }

        if (!jsonObjectDetail.isNull("location")) {
            eventModel.locationTitle = jsonObjectDetail.getJSONObject("location").getString("title")

            if (!jsonObjectDetail.getJSONObject("location").isNull("description")) {
                eventModel.locationDescription = jsonObjectDetail.getJSONObject("location").getString("description")
            } else {
                eventModel.locationDescription = ""
            }

            if (!jsonObjectDetail.getJSONObject("location").isNull("coordinate")){
                eventModel.locationType = jsonObjectDetail.getJSONObject("location").getJSONObject("coordinate").getString("type")
                when (eventModel.locationType) {
                    "Point" -> {
                        val lat = jsonObjectDetail.getJSONObject("location").getJSONObject("coordinate").getJSONArray("coordinates").getDouble(0)
                        val long = jsonObjectDetail.getJSONObject("location").getJSONObject("coordinate").getJSONArray("coordinates").getDouble(1)
                        eventModel.location.add(GeoPoint(lat, long))
                    }
                    "Line" -> {
                        // prepared for implementation
                    }
                    "Polygon" -> {
                        // prepared for implementation
                    }
                }
            } else {
                eventModel.locationType = context.getString(R.string.event_default_property_location_type_no_location)
            }
        } else {
            eventModel.locationType = context.getString(R.string.event_default_property_location_type_no_location)
        }
        return eventModel
    }

    fun parseResponseGetEvents(apiResponse: String): ArrayList<EventModel> {
        val arrayListEvents: ArrayList<EventModel> = ArrayList()
        // convert to a json array
        val apiResponseJsonArray = JSONArray(apiResponse)

        for (eventArrayPosition in 0 until apiResponseJsonArray.length()) {
            val jsonObjectDetail: JSONObject = apiResponseJsonArray.getJSONObject(eventArrayPosition)

            val eventModel = parseEventReducedInfo(jsonObjectDetail)

            arrayListEvents.add(eventModel)
        }
        return arrayListEvents
    }

    fun parseResponseGetEvent(apiResponse: String): EventModel {
        val apiResponseJsonArray = JSONArray(apiResponse)

        val jsonObjectDetail: JSONObject = apiResponseJsonArray.getJSONObject(0)

        val eventModel = parseEventReducedInfo(jsonObjectDetail)

        if (!jsonObjectDetail.isNull("morelinks") && jsonObjectDetail.getJSONObject("morelinks").getJSONArray("link").length() > 0 ) {
            val moreLinksArray = jsonObjectDetail.getJSONObject("morelinks").getJSONArray("link")
            for (i in 0 until moreLinksArray.length()) {
                val moreLink = MoreLink()
                moreLink.uuid = moreLinksArray.getJSONObject(i).getString("uuid")
                moreLink.title = moreLinksArray.getJSONObject(i).getString("title")
                moreLink.url = moreLinksArray.getJSONObject(i).getString("url")

                eventModel.moreLinks.add(moreLink)
            }
        }

        if (!jsonObjectDetail.isNull("district") && !jsonObjectDetail.getJSONObject("district").isNull("title")) {
            eventModel.locationDistrict = jsonObjectDetail.getJSONObject("district").getString("title")
        }

        return eventModel
    }

    fun parseResponseGetSections(apiResponse: String): ArrayList<SectionModel> {
        val arrayListSections: ArrayList<SectionModel> = ArrayList()
        val apiResponseJsonArray = JSONArray(apiResponse)

        for (sectionArrayPosition in 0 until apiResponseJsonArray.length()) {
            val jsonObjectDetail: JSONObject = apiResponseJsonArray.getJSONObject(sectionArrayPosition)
            val sectionModel = SectionModel()

            sectionModel.uuid = jsonObjectDetail.getString("uuid")
            sectionModel.title = jsonObjectDetail.getString("title")

            arrayListSections.add(sectionModel)
        }
        return arrayListSections
    }

    fun parseResponseGetEventTypes(apiResponse: String): ArrayList<EventTypeModel> {
        val arrayListEventTypes: ArrayList<EventTypeModel> = ArrayList()
        val apiResponseJsonArray = JSONArray(apiResponse)
        for (eventTypeArrayPosition in 0 until apiResponseJsonArray.length()) {
            val jsonObjectDetail: JSONObject = apiResponseJsonArray.getJSONObject(eventTypeArrayPosition)
            val eventTypeModel = EventTypeModel()

            eventTypeModel.uuid = jsonObjectDetail.getString("uuid")
            eventTypeModel.title = jsonObjectDetail.getString("title")

            arrayListEventTypes.add(eventTypeModel)
        }

        return arrayListEventTypes
    }

    fun parseResponseGetDistricts(apiResponse: String): ArrayList<DistrictModel> {
        val arrayListDistricts: ArrayList<DistrictModel> = ArrayList()
        val apiResponseJsonArray = JSONArray(apiResponse)
        for (eventTypeArrayPosition in 0 until apiResponseJsonArray.length()) {
            val jsonObjectDetail: JSONObject = apiResponseJsonArray.getJSONObject(eventTypeArrayPosition)
            val districtModel = DistrictModel()

            districtModel.uuid = jsonObjectDetail.getString("uuid")
            districtModel.title = jsonObjectDetail.getString("title")

            arrayListDistricts.add(districtModel)
        }

        return arrayListDistricts
    }

}