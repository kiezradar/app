package de.fraunhofer.fokus.kiezradar

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.ImageButton
import android.widget.ListView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import de.fraunhofer.fokus.kiezradar.LocationHandler.checkPermissionForLocation
import de.fraunhofer.fokus.kiezradar.LocationHandler.startLocationUpdates
import de.fraunhofer.fokus.kiezradar.datamodels.DistrictModel
import de.fraunhofer.fokus.kiezradar.datamodels.EventModel
import de.fraunhofer.fokus.kiezradar.datamodels.EventTypeModel
import de.fraunhofer.fokus.kiezradar.datamodels.SectionModel
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler.loadSubFragment
import de.fraunhofer.fokus.kiezradar.ui.discover.DiscoverFragment
import de.fraunhofer.fokus.kiezradar.ui.favorites.FavoritesFragment
import de.fraunhofer.fokus.kiezradar.ui.map.MapFragment
import de.fraunhofer.fokus.kiezradar.ui.search.SearchFragment
import de.fraunhofer.fokus.kiezradar.ui.settings.SettingsFragment
import org.json.JSONException
import org.osmdroid.util.GeoPoint
import java.io.FileNotFoundException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavigation: BottomNavigationView

    companion object {
        var actualLocationCoordinates: GeoPoint = GeoPoint(52.52437, 13.41053)
        var eventListActivePeriodSortedByDate: ArrayList<EventModel> = ArrayList()
        var eventListFilteredSortedByDate: ArrayList<EventModel> = ArrayList()
        var eventListFilteredSortedByLocation: ArrayList<EventModel> = ArrayList()
        var eventListFavorites: ArrayList<EventModel> = ArrayList()
        var arrayListSections: ArrayList<SectionModel> = ArrayList()
        var arrayListEventTypes: ArrayList<EventTypeModel> = ArrayList()
        var arrayListDistricts: ArrayList<DistrictModel> = ArrayList()
        var requestPermissionLocation = 10
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadSubFragment(
            supportFragmentManager,
            DiscoverFragment(),
            resources.getString(R.string.tag_discover_fragment),
            ""
        )

        bottomNavigation = findViewById(R.id.bottomNav)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        // Location updates
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
        }
        if (checkPermissionForLocation(this, this)) {
            startLocationUpdates(this)
        }

        // initialize reduced data set for startup and reload old data
        thread {
            val dataHandler = DataHandler(this, this)
            try {
                dataHandler.restoreFavorites()
                dataHandler.reloadDataLastUse()
            }
            catch (exc: FileNotFoundException) {
                Debugger.logError("MainActivity","File in internal storage does not exist or is not accessible")
            }
            catch (exc: JSONException) {
                Debugger.logError("MainActivity","JSON format is not manageable")
                exc.printStackTrace()
            }
            catch (exc: Exception) {
                exc.printStackTrace()
            }
        }
        // get sections, eventTypes, districts
        thread {
            val dataHandler = DataHandler(this, this)
            try {
                dataHandler.getAllSections()
                dataHandler.getAllEventTypes()
                dataHandler.getAllDistricts()
            }
            catch (exc: JSONException) {
                Debugger.logError("MainActivity","JSON format is not manageable")
                exc.printStackTrace()
            }
            catch (exc: Exception) {
                exc.printStackTrace()
            }
        }
        // get all events
        thread {
            val dataHandler = DataHandler(this, this)
            try {
                dataHandler.getAllActiveEvents()
            }
            catch (exc: JSONException) {
                Debugger.logError("MainActivity","JSON format is not manageable")
                exc.printStackTrace()
            }
            catch (exc: Exception) {
                exc.printStackTrace()
            }
        }

    }

    // init bottom nav item listener
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_discover -> {
                loadSubFragment(
                    supportFragmentManager,
                    DiscoverFragment(),
                    resources.getString(R.string.tag_discover_fragment),
                    ""
                )
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_map -> {
                loadSubFragment(
                    supportFragmentManager,
                    MapFragment(),
                    resources.getString(R.string.tag_map_fragment),
                    ""
                )
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_favorites -> {
                loadSubFragment(
                    supportFragmentManager,
                    FavoritesFragment(),
                    resources.getString(R.string.tag_favorites_fragment),
                    ""
                )
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_search -> {
                loadSubFragment(
                    supportFragmentManager,
                    SearchFragment(),
                    resources.getString(R.string.tag_search_fragment),
                    ""
                )
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_settings -> {
                loadSubFragment(
                    supportFragmentManager,
                    SettingsFragment(),
                    resources.getString(R.string.tag_settings_fragment),
                    ""
                )
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onDestroy() {
        val dataHandler = DataHandler(this, this)
        dataHandler.storeFavorites()
        super.onDestroy()
    }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
            .setCancelable(false)
            .setPositiveButton("Yes") { _, _ ->
                startActivityForResult(
                    Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 11
                )
            }
            .setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
                finish()
            }
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestPermissionLocation) {
            requestPermissionLocation -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates(this)
                } else {
                    Toast.makeText(this@MainActivity, "Permission Denied", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun favoritesButtonClicked(view: View) {
        val fragmentTag = view.findFragment<Fragment>().tag
        lateinit var listView: ListView
        lateinit var eventList: ArrayList<EventModel>
        val button: ImageButton = view as ImageButton

        when (fragmentTag) {
            resources.getString(R.string.tag_discover_current_fragment) -> {
                listView = findViewById(R.id.eventListViewDiscoverCurrent)
                eventList = eventListFilteredSortedByDate
            }
            resources.getString(R.string.tag_discover_nearby_fragment) -> {
                listView = findViewById(R.id.eventListViewDiscoverNearBy)
                eventList = eventListFilteredSortedByLocation
            }
            resources.getString(R.string.tag_discover_popular_fragment) -> {
                listView = findViewById(R.id.eventListViewDiscoverPopular)
                eventList = eventListActivePeriodSortedByDate
            }
            resources.getString(R.string.tag_discover_private_fragment) -> {
                // new discover fragment prepared
            }
            resources.getString(R.string.tag_favorites_fragment) -> {
                listView = findViewById(R.id.eventListViewFavorites)
                eventList = eventListFavorites
            }
            resources.getString(R.string.tag_search_fragment) -> {
                // search fragment prepared
            }
        }

        val positionInList: Int = listView.getPositionForView(view)
        val eventUuid: String = eventList[positionInList].uuid

        if (!eventListFavorites.any { eventObject -> eventObject.uuid == eventUuid}) {
            eventListFavorites.add(eventList[positionInList])
            button.setImageResource(R.drawable.ic_star_gold_filled)
        } else if (eventListFavorites.any { eventObject -> eventObject.uuid == eventUuid}) {
            eventListFavorites.removeIf { it.uuid == eventUuid }
            button.setImageResource(R.drawable.ic_star)

        }
    }

    fun selectNotificationsFragment(){
        bottomNavigation.menu.setGroupCheckable(0, true, false)
        for (i in 0 until bottomNavigation.menu.size()) {
            bottomNavigation.menu.getItem(i).isChecked = false
        }
        bottomNavigation.menu.setGroupCheckable(0, true, true)
    }

    fun selectFragmentInBottomNav(itemIndex: Int){
        bottomNavigation.menu.getItem(itemIndex).isChecked = true
    }

    fun refreshFragment(fragmentTag: String) {
        runOnUiThread {FragmentHandler.reloadFragment(this.supportFragmentManager, fragmentTag)}
    }

    fun isNetworkAvailable():Boolean{
        val conManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val internetInfo = conManager.activeNetworkInfo
        return internetInfo!=null && internetInfo.isConnected
    }
}
