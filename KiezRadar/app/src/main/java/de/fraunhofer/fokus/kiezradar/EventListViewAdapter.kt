package de.fraunhofer.fokus.kiezradar

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import com.squareup.picasso.Picasso
import de.fraunhofer.fokus.kiezradar.datamodels.EventModel
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.collections.ArrayList


class EventListViewAdapter (
    private val context: Context,
    private val arrayListEventDetails: ArrayList<EventModel>) : BaseAdapter() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return arrayListEventDetails.size
    }

    override fun getItem(position: Int): Any {
        return arrayListEventDetails[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val listRowHolder: DiscoverFragmentListRowHolder
        if (convertView == null) {
            view = this.layoutInflater.inflate(R.layout.event_list_layout, parent, false)
            listRowHolder = DiscoverFragmentListRowHolder(view)
            view.tag = listRowHolder



        } else {
            view = convertView
            listRowHolder = view.tag as DiscoverFragmentListRowHolder
        }

        if (arrayListEventDetails.isNotEmpty()) {
            listRowHolder.eventTitle.text = arrayListEventDetails[position].title
            listRowHolder.eventDescription.text = arrayListEventDetails[position].description
            if (arrayListEventDetails[position].imageUrl.isNotEmpty()){
                Picasso.get().load(arrayListEventDetails[position].imageUrl).into(listRowHolder.eventImage)
            } else {
                listRowHolder.eventImage.setImageResource(R.drawable.ic_no_image)
            }
            val dateFormatterShort = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMANY)
            val endDate = arrayListEventDetails[position].periodEnd.format(dateFormatterShort)
            val daysUntilEndDate = LocalDateTime.now().until(arrayListEventDetails[position].periodEnd, ChronoUnit.DAYS).toString()
            listRowHolder.textViewEventTimeUntilDeadline.text = context.getString(R.string.event_layout_text_view_event_time_until_end, endDate, daysUntilEndDate)


            val idListFavoritesEvents = MainActivity.eventListFavorites.map { it.uuid }
            if (idListFavoritesEvents.contains(arrayListEventDetails[position].uuid)) {
                listRowHolder.favoritesButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_gold_filled))
            }

            // add event type tile
            if (arrayListEventDetails[position].eventType.isNotEmpty()) {
                val eventTypeTextView = TextView(context)
                eventTypeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10.0F)
                eventTypeTextView.text = arrayListEventDetails[position].eventType
                eventTypeTextView.setTextColor(ContextCompat.getColor(context, R.color.white))
                // set background shape with default background color
                eventTypeTextView.setBackgroundResource(R.drawable.list_view_event_section_background)
                // set background color of tiles depending on eventType
                when (arrayListEventDetails[position].eventType) {
                    context.getString(R.string.event_type_participation_project) -> {
                        eventTypeTextView.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.participation_project))
                    }
                    context.getString(R.string.event_type_meeting) -> {
                        eventTypeTextView.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.meeting))
                    }
                    context.getString(R.string.event_type_neighborhood_community) -> {
                        eventTypeTextView.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.neighborhood_community))
                    }
                    context.getString(R.string.event_type_citizens_survey) -> {
                        eventTypeTextView.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.citizens_survey))
                    }
                    context.getString(R.string.event_type_building_project) -> {
                        eventTypeTextView.backgroundTintList = ColorStateList.valueOf(context.getColor(R.color.building_project))
                    }
                }
                eventTypeTextView.setPadding(20)
                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                eventTypeTextView.layoutParams = params

                listRowHolder.eventTypeContainer.removeAllViewsInLayout()
                listRowHolder.eventTypeContainer.addView(eventTypeTextView)
            }
        }

        return view
    }
}

private class DiscoverFragmentListRowHolder(row: View?) {
    val eventTitle: TextView = row?.findViewById(R.id.event_layout_text_view_event_title) as TextView
    val eventDescription: TextView = row?.findViewById(R.id.event_layout_text_view_event_description) as TextView
    val eventImage: ImageView = row?.findViewById(R.id.event_layout_image_view_event_image) as ImageView
    val eventTypeContainer: LinearLayout = row?.findViewById(R.id.event_layout_text_view_event_type_container) as LinearLayout
    val textViewEventTimeUntilDeadline: TextView = row?.findViewById(R.id.event_layout_text_view_event_time_until_end) as TextView
    val favoritesButton: ImageButton = row?.findViewById(R.id.event_layout_list_view_button_star) as ImageButton
}
