package de.fraunhofer.fokus.kiezradar.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment

object FragmentHandler {
    fun loadSubFragment(manager: FragmentManager, fragment: Fragment, targetFragmentTag: String, sourceFragmentTag: String) {
        val transaction = manager.beginTransaction()
        transaction.replace(R.id.container, fragment, targetFragmentTag)

        if (targetFragmentTag == "FILTER_FRAGMENT") {
            FilterFragment.tagCallingFragment = sourceFragmentTag
        }

        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun reloadFragment(manager: FragmentManager, fragmentTag: String) {
        val currentFragment: Fragment? = manager.findFragmentByTag(fragmentTag)
        val transaction = manager.beginTransaction()

        if (currentFragment != null) {
            transaction.detach(currentFragment)
            transaction.attach(currentFragment)
            transaction.commit()
        }
    }
}