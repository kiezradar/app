package de.fraunhofer.fokus.kiezradar.ui.event_details

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.button.MaterialButton
import com.squareup.picasso.Picasso
import de.fraunhofer.fokus.kiezradar.Debugger
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.datamodels.EventModel
import de.fraunhofer.fokus.kiezradar.datamodels.MoreLink
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler
import de.fraunhofer.fokus.kiezradar.ui.TilesHandler
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterViewModel
import de.fraunhofer.fokus.kiezradar.ui.notifications.NotificationsFragment
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class EventDetailsFragment : Fragment() {

    private lateinit var filterViewModel: FilterViewModel
    private lateinit var setFiltersContainer: LinearLayout
    private lateinit var localListSetFilters: ArrayList<String>
    private lateinit var eventDetailsViewModel: EventDetailsViewModel
    lateinit var event: EventModel
    private lateinit var eventDetailsViewTitleView: TextView
    private lateinit var eventDetailsViewImageView: ImageView
    private lateinit var eventDetailsViewDescriptionView: TextView
    private lateinit var eventDetailsImageButtonFavorite: ImageButton
    private lateinit var eventDetailsTextViewLocationTitle: TextView
    private lateinit var eventDetailsScrollViewLocationDescription: HorizontalScrollView
    private lateinit var eventDetailsTextViewLocationDescription: TextView
    private lateinit var eventDetailsTextViewActivityPeriod: TextView
    private lateinit var eventDetailsContainerMoreLinks: LinearLayout
    private lateinit var localMoreLinksList: ArrayList<String>

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_event_details, container, false)

        eventDetailsViewModel = activity?.run {
            ViewModelProvider(this).get(EventDetailsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        filterViewModel = activity?.run {
            ViewModelProvider(this).get(FilterViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val filterButton: ImageButton = root.findViewById(R.id.button_filter)
        filterButton.setOnClickListener {
            FragmentHandler.loadSubFragment(
                activity?.supportFragmentManager!!,
                FilterFragment(),
                resources.getString(R.string.tag_filter_fragment),
                resources.getString(R.string.tag_event_details_fragment)
            )
        }

        val notificationsButton: ImageButton = root.findViewById(R.id.button_notifications)
        notificationsButton.setOnClickListener {
            FragmentHandler.loadSubFragment(
                activity?.supportFragmentManager!!,
                NotificationsFragment(),
                resources.getString(R.string.tag_notifications_fragment),
                resources.getString(R.string.tag_event_details_fragment)
            )
        }

        setFiltersContainer = root.findViewById(R.id.top_bar_event_filter_container) as LinearLayout

        filterViewModel.radius.observe(viewLifecycleOwner, radiusObserver)
        filterViewModel.periodOfTimeStart.observe(viewLifecycleOwner, periodOfTimeStartObserver)
        filterViewModel.periodOfTimeEnd.observe(viewLifecycleOwner, periodOfTimeEndObserver)
        localListSetFilters = ArrayList()
        filterViewModel.setFilters.observe(viewLifecycleOwner, setFiltersObserver)

        localMoreLinksList = ArrayList()

        eventDetailsViewTitleView = root.findViewById(R.id.event_details_text_view_title)
        eventDetailsViewImageView = root.findViewById(R.id.event_details_image_view_image)
        eventDetailsViewDescriptionView = root.findViewById(R.id.event_details_text_view_description)
        eventDetailsImageButtonFavorite = root.findViewById(R.id.event_details_button_favorite)
        eventDetailsTextViewLocationTitle = root.findViewById(R.id.event_details_text_view_location_title)
        eventDetailsScrollViewLocationDescription = root.findViewById(R.id.event_details_scrollview_location_description)
        eventDetailsTextViewLocationDescription = root.findViewById(R.id.event_details_text_view_location_description)
        eventDetailsTextViewActivityPeriod = root.findViewById(R.id.event_details_text_view_activity_period)
        eventDetailsContainerMoreLinks = root.findViewById(R.id.event_details_layout_container_buttons_more_links)

        val idListFavoritesEvents = MainActivity.eventListFavorites.map { it.uuid }
        if (idListFavoritesEvents.contains(eventDetailsViewModel.uuid.value)) {
            eventDetailsImageButtonFavorite.setImageResource(R.drawable.ic_star_gold_filled)
        }

        eventDetailsViewModel.uuid.observe(viewLifecycleOwner, uuidEventDetailsVMObserver)
        eventDetailsViewModel.title.observe(viewLifecycleOwner, titleEventDetailsVMObserver)
        eventDetailsViewModel.image.observe(viewLifecycleOwner, imageEventDetailsVMObserver)
        eventDetailsViewModel.description.observe(viewLifecycleOwner, descriptionEventDetailsVMObserver)
        eventDetailsViewModel.moreLinks.observe(viewLifecycleOwner, moreLinksEventDetailsObserver)

        eventDetailsImageButtonFavorite.setOnClickListener(favoriteButtonClickListener)


        return root
    }

    private val radiusObserver: Observer<String> = Observer { radius ->
        if ((radius != "") && !filterViewModel.showAll.value!!) {
            context?.let { context ->
                TilesHandler.addFilterTileToContainer(
                    radius,
                    "radius",
                    "" ,
                    radius.hashCode(),
                    context,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val periodOfTimeStartObserver: Observer<String> = Observer { periodStart ->
        if (periodStart != "") {
            context?.let {
                TilesHandler.addFilterTileToContainer(periodStart,
                    "period",
                    "start",
                    periodStart.hashCode(),
                    it,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val periodOfTimeEndObserver: Observer<String> = Observer { periodEnd ->
        if (periodEnd != "") {
            context?.let {
                TilesHandler.addFilterTileToContainer(periodEnd,
                    "period",
                    "end",
                    periodEnd.hashCode(),
                    it,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val setFiltersObserver: Observer<MutableList<String>> = Observer { filters ->
        for (filter in filters) {
            if (!localListSetFilters.contains(filter)) {
                // add selected filter to setFiltersList
                localListSetFilters.add(filter)
                // add view
                context?.let { context ->
                    TilesHandler.addFilterTileToContainer(
                        filter,
                        "other",
                        "",
                        filter.hashCode(),
                        context,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel)
                }
            }
        }
        val filtersToRemove: ArrayList<String> = ArrayList()
        for (filterLocal in localListSetFilters) {
            if (!filters.contains(filterLocal)) {
                filtersToRemove.add(filterLocal)
            }
        }
        localListSetFilters.removeAll(filtersToRemove)

    }

    private val uuidEventDetailsVMObserver: Observer<String> = Observer { uuid ->
        event = MainActivity.eventListFilteredSortedByDate.find { it.uuid == uuid }!!
        // transfer geoCoordinates into address and set editText input
        when (event.locationType) {
            getString(R.string.event_default_property_location_type_no_location) -> {
                eventDetailsTextViewLocationTitle.text = resources.getString(R.string.event_details_text_view_location_no_location)
            }
            "Point" -> {
                eventDetailsTextViewLocationTitle.text = event.locationTitle
                if (event.locationDescription != "") {
                    eventDetailsTextViewLocationDescription.text = getString(R.string.event_layout_text_view_event_location_description, event.locationDescription)
                }
                else {
                    eventDetailsScrollViewLocationDescription.visibility = View.GONE
                }
            }
        }
        val daysUntilEnd: Int = LocalDateTime.now().until(event.periodEnd, ChronoUnit.DAYS).toInt()
        if (daysUntilEnd >= 0){
            eventDetailsTextViewActivityPeriod.text = resources.getString(R.string.event_details_text_view_activity_period_activ)
        } else {
            eventDetailsTextViewActivityPeriod.text = resources.getString(R.string.event_details_text_view_activity_period_not_activ)
        }
    }

    private val titleEventDetailsVMObserver: Observer<String> = Observer {
        eventDetailsViewTitleView.text = eventDetailsViewModel.title.value
    }

    private val imageEventDetailsVMObserver: Observer<String> = Observer {
        if (eventDetailsViewModel.image.value != null) {
            try {
                Picasso.get().load(eventDetailsViewModel.image.value).into(eventDetailsViewImageView)
            } catch (e : IllegalArgumentException) {
                Debugger.logString("EventDetailsFragment Picasso Handling", eventDetailsViewModel.image.value.toString())
            }

        } else {
            eventDetailsViewImageView.setImageResource(R.drawable.ic_no_image)
        }
    }

    private val descriptionEventDetailsVMObserver: Observer<String> = Observer {
        eventDetailsViewDescriptionView.text = eventDetailsViewModel.description.value
    }


    private val favoriteButtonClickListener = View.OnClickListener { _ ->
        val eventUuid: String? = eventDetailsViewModel.uuid.value

        if (!MainActivity.eventListFavorites.any { eventObject -> eventObject.uuid == eventUuid }) {
            MainActivity.eventListFavorites.add(event)
            eventDetailsImageButtonFavorite.setImageResource(R.drawable.ic_star_gold_filled)
        } else {
            MainActivity.eventListFavorites.removeIf { it.uuid == eventUuid }
            eventDetailsImageButtonFavorite.setImageResource(R.drawable.ic_star)
        }

    }

    private val moreLinksEventDetailsObserver: Observer<MutableList<MoreLink>> = Observer { arrayListMoreLinks ->
        for (moreLink in arrayListMoreLinks) {

            if (moreLink.uuid !in localMoreLinksList) {
                val newContext = ContextThemeWrapper(
                    context,
                    R.style.eventDetailsButtonFurtherInformation
                )
                val materialButton = MaterialButton(newContext)

                materialButton.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                materialButton.text = moreLink.title

                materialButton.width = 0

                val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1f)
                layoutParams.setMargins(10, 0, 10, 0)
                layoutParams.gravity = Gravity.CENTER

                materialButton.layoutParams = layoutParams

                materialButton.setOnClickListener {
                    val openURL = Intent(Intent.ACTION_VIEW)
                    openURL.data = Uri.parse(moreLink.url)
                    startActivity(openURL)
                }

                eventDetailsContainerMoreLinks.addView(materialButton)

                localMoreLinksList.add(moreLink.uuid)
            }

        }

    }

}