package de.fraunhofer.fokus.kiezradar.ui.discover

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import de.fraunhofer.fokus.kiezradar.DataHandler
import de.fraunhofer.fokus.kiezradar.EventListViewAdapter
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsHandler
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsViewModel
import kotlin.concurrent.thread

class DiscoverNearByFragment : Fragment() {

    private lateinit var listViewDiscoverNearBy: ListView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_discover_near_by, container, false)

        val eventDetailsViewModel: EventDetailsViewModel = activity?.run {
            ViewModelProvider(this).get(EventDetailsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        // find listView and set adapter + content
        listViewDiscoverNearBy = root.findViewById(R.id.eventListViewDiscoverNearBy) as ListView
        if (MainActivity.eventListFilteredSortedByLocation.isNotEmpty()) {
            val adapter = EventListViewAdapter(requireContext(), MainActivity.eventListFilteredSortedByLocation)
            listViewDiscoverNearBy.adapter = adapter
        } else {
            val listViewContainer = root.findViewById<ConstraintLayout>(R.id.eventListViewDiscoverNearByEmptyContainer)
            val textView = TextView(context)
            textView.setText(R.string.discover_list_empty)
            textView.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
            listViewContainer.addView(textView)
        }

        listViewDiscoverNearBy.setOnItemClickListener { _, _, position, _ ->
            // use companion object to get event
            val selectedEventInEventDetails = MainActivity.eventListFilteredSortedByLocation[position]

            EventDetailsHandler.transferAndRequestData(
                selectedEventInEventDetails,
                eventDetailsViewModel,
                requireActivity())
        }

        val swipeRefreshLayout = root.findViewById<SwipeRefreshLayout>(R.id.swipe_refresh_discover)
        swipeRefreshLayout.setOnRefreshListener {
            thread {
                context?.let { ctx -> DataHandler(ctx, requireActivity()).getFilteredEvents() }
            }
        }

        return root
    }
}