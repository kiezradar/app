package de.fraunhofer.fokus.kiezradar.ui.event_details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.fraunhofer.fokus.kiezradar.datamodels.MoreLink

class EventDetailsViewModel : ViewModel() {

    private val _uuid = MutableLiveData<String>().apply {
        value = "This is the uuid"
    }
    val uuid: LiveData<String> = _uuid
    fun setUuid(t: String) {
        _uuid.value = t
    }

    private val _title = MutableLiveData<String>().apply {
        value = "This is the title"
    }
    val title: LiveData<String> = _title
    fun setTitle(t: String) {
        _title.value = t
    }

    private val _description = MutableLiveData<String>().apply {
        value = "This is the description"
    }
    val description: LiveData<String> = _description
    fun setDescription(d: String) {
        _description.value = d
    }

    private val _image = MutableLiveData<String>().apply {
        value = "This is the image URI"
    }
    val image: LiveData<String> = _image
    fun setImageUri(u: String) {
        _image.value = u
    }

    private val _moreLinks = MutableLiveData<MutableList<MoreLink>>().apply {
    }

    val moreLinks: LiveData<MutableList<MoreLink>> = _moreLinks

    private var moreLinksList = ArrayList<MoreLink>()

    fun addMoreLink(moreLink: MoreLink) {
        if (!moreLinksList.map {it.uuid}.contains(moreLink.uuid)) {
            moreLinksList.add(moreLink)
            _moreLinks.value = moreLinksList
        }
    }

    fun resetMoreLinkList() {
        moreLinksList = ArrayList()
        _moreLinks.value = moreLinksList
    }

}