package de.fraunhofer.fokus.kiezradar.ui

import android.app.Activity
import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.view.marginRight
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import de.fraunhofer.fokus.kiezradar.DataHandler
import de.fraunhofer.fokus.kiezradar.Debugger
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterViewModel
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.concurrent.thread


object TilesHandler {

    private val dateFormatterLong = DateTimeFormatter.ofPattern("EEEE, dd. MMMM yyyy", Locale.GERMANY)
    private val dateFormatterShort = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMANY)

    fun addFilterTileToContainer(tileString: String, tileType: String, tileIndicator: String, id: Int, context: Context, activity: Activity, setFiltersContainer: LinearLayout, filterViewModel: FilterViewModel) {
        val fragmentTag = setFiltersContainer.findFragment<Fragment>().tag
        // add filter option boxes
        val setFilterLayout = ConstraintLayout(context)
        setFilterLayout.layoutParams = ConstraintLayout.LayoutParams(
            ConstraintLayout.LayoutParams.WRAP_CONTENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        if (fragmentTag == context.resources.getString(R.string.tag_filter_fragment)) {
            setFilterLayout.layoutParams.height = context.resources.getDimensionPixelSize(R.dimen.filter_fragment_set_filter_container_height)
        } else {
            setFilterLayout.layoutParams.height = context.resources.getDimensionPixelSize(R.dimen.filter_tiles_set_filter_container_height)
        }
        setFilterLayout.id = id
        setFilterLayout.setBackgroundResource(R.drawable.filter_fragment_set_filters_background)

        val setFilterTextView = TextView(context)
        setFilterTextView.id = id + 100
        if (fragmentTag == context.resources.getString(R.string.tag_filter_fragment)) {
            setFilterTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, context.resources.getDimension(R.dimen.filter_fragment_set_filter_text_size))
        } else {
            setFilterTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, context.resources.getDimension(R.dimen.filter_tiles_set_filter_text_size))
        }

        when (tileType) {
            "period" -> {
                val date = LocalDate.parse(tileString, dateFormatterLong)
                val formattedDate = date.format(dateFormatterShort)

                when (tileIndicator) {
                    "start" -> {
                        setFilterLayout.id = "filterFragmentPeriodStartTileLayout".hashCode()
                        setFilterTextView.id = "filterFragmentPeriodStartTileTextView".hashCode()
                        setFilterTextView.text = activity.getString(R.string.text_view_period_time_start_tile, formattedDate)
                    }
                    "end" -> {
                        setFilterLayout.id = "filterFragmentPeriodEndTileLayout".hashCode()
                        setFilterTextView.id = "filterFragmentPeriodEndTileTextView".hashCode()
                        setFilterTextView.text = activity.getString(R.string.text_view_period_time_end_tile, formattedDate)
                    }
                    else -> {
                        Debugger.logWarning("TILESHANDLER", "Case should not be reached")
                    }
                }
            }
            "radius" -> {
                setFilterLayout.id = "filterFragmentRadiusTileLayout".hashCode()
                setFilterTextView.id = "filterFragmentRadiusTileTextView".hashCode()
                setFilterTextView.text = activity.getString(R.string.text_view_radius_tile, tileString)
            }
            "other" -> {
                setFilterTextView.text = tileString
            }
        }

        context.let { ContextCompat.getColor(it, R.color.black) }.let {
            setFilterTextView.setTextColor(
                it
            )
        }
        setFilterTextView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        setFilterLayout.addView(setFilterTextView)

        // add delete button
        val deleteButton = ImageButton(context)
        deleteButton.id = id + 1000
        deleteButton.marginRight
        deleteButton.setBackgroundResource(R.drawable.button_background_rounded_primarydark)
        deleteButton.setImageResource(R.drawable.ic_delete)

        deleteButton.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        if (fragmentTag == context.resources.getString(R.string.tag_filter_fragment)) {
            deleteButton.layoutParams.height = context.resources.getDimensionPixelSize(R.dimen.filter_fragment_set_filter_delete_button_size)
            deleteButton.layoutParams.width = context.resources.getDimensionPixelSize(R.dimen.filter_fragment_set_filter_delete_button_size)
        } else {
            deleteButton.layoutParams.height = context.resources.getDimensionPixelSize(R.dimen.filter_tiles_set_filter_delete_button_size)
            deleteButton.layoutParams.width = context.resources.getDimensionPixelSize(R.dimen.filter_tiles_set_filter_delete_button_size)
        }
        deleteButton.scaleType = ImageView.ScaleType.FIT_CENTER
        deleteButton.setPadding(0)

        when (tileType) {
            "period" -> {
                deleteButton.id = "filterFragmentPeriodTileDeleteButton$tileString".hashCode()

                deleteButton.setOnClickListener {
                    setFiltersContainer.removeViewInLayout(setFilterLayout)
                    when (tileIndicator) {
                        "start" -> {
                            FilterFragment.setPeriodStart = LocalDate.of(0, 1, 1)
                            filterViewModel.setPeriodOfTimeStart("")
                            filterViewModel.setPeriodOfTimeSelection("end")
                        }
                        "end" -> {
                            FilterFragment.setPeriodEnd = LocalDate.of(0, 1, 1)
                            filterViewModel.setPeriodOfTimeEnd("")
                            filterViewModel.setPeriodOfTimeSelection("start")
                        }
                        else -> {
                            Debugger.logWarning("TILESHANDLER", "Case should not be reached")
                        }
                    }

                    if (fragmentTag == context.resources.getString(R.string.tag_filter_fragment)) {
                        setFiltersContainer.findFragment<Fragment>().hideKeyboard()
                    } else {
                        thread {
                            DataHandler(context, activity).getFilteredEvents()
                        }
                    }
                    // reload filter container view
                    setFiltersContainer.invalidate()
                }
            }
            "radius" -> {
                deleteButton.id = "filterFragmentRadiusTileDeleteButton".hashCode()

                deleteButton.setOnClickListener {
                    setFiltersContainer.removeViewInLayout(setFilterLayout)
                    filterViewModel.setShowAll(true)
                    filterViewModel.setRadius("")
                    FilterFragment.setRadius = 0
                    if (fragmentTag == context.resources.getString(R.string.tag_filter_fragment)) {
                        setFiltersContainer.findFragment<Fragment>().hideKeyboard()
                    } else {
                        thread {
                            DataHandler(context, activity).getFilteredEvents()
                        }
                    }
                    // reload filter container view
                    setFiltersContainer.invalidate()
                }
            }
            "other" -> {
                deleteButton.setOnClickListener {
                    filterViewModel.removeFilter(tileString)
                    setFiltersContainer.removeViewInLayout(setFilterLayout)
                    FilterFragment.setFiltersList.remove(tileString)
                    if (fragmentTag == context.resources.getString(R.string.tag_filter_fragment)) {
                        val searchView =
                            activity.findViewById<SearchView>(R.id.top_bar_layout_filter_search_view)
                        // reset search view (work around)
                        searchView.onActionViewCollapsed()
                        searchView.isIconified = false
                        setFiltersContainer.findFragment<Fragment>().hideKeyboard()
                    } else {
                        thread {
                            DataHandler(context, activity).getFilteredEvents()
                        }
                    }
                    // reload filter container view
                    setFiltersContainer.invalidate()
                }
            }
        }

        setFilterLayout.addView(deleteButton)

        // set constraints
        val constraintSet = ConstraintSet()
        constraintSet.clone(setFilterLayout)
        constraintSet.connect(
            setFilterTextView.id,
            ConstraintSet.LEFT,
            ConstraintSet.PARENT_ID,
            ConstraintSet.LEFT,
             if (fragmentTag == context.resources.getString(R.string.tag_filter_fragment)) {
                 context.resources.getDimensionPixelSize(R.dimen.filter_fragment_set_filter_delete_button_size)
             } else {
                 context.resources.getDimensionPixelSize(R.dimen.filter_tiles_set_filter_delete_button_size)
             }
        )
        constraintSet.connect(
            setFilterTextView.id,
            ConstraintSet.TOP,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            0
        )
        constraintSet.connect(
            setFilterTextView.id,
            ConstraintSet.BOTTOM,
            ConstraintSet.PARENT_ID,
            ConstraintSet.BOTTOM,
            0
        )
        constraintSet.connect(
            deleteButton.id,
            ConstraintSet.LEFT,
            setFilterTextView.id,
            ConstraintSet.RIGHT,
            0
        )
        constraintSet.connect(
            deleteButton.id,
            ConstraintSet.TOP,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            0
        )
        constraintSet.connect(
            deleteButton.id,
            ConstraintSet.RIGHT,
            ConstraintSet.PARENT_ID,
            ConstraintSet.RIGHT,
            0
        )
        constraintSet.applyTo(setFilterLayout)

        setFiltersContainer.addView(setFilterLayout)
    }

    private fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

}