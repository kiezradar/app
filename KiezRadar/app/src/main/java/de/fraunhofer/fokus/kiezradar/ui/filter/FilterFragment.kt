package de.fraunhofer.fokus.kiezradar.ui.filter

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.database.Cursor
import android.database.MatrixCursor
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.BaseColumns
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.CursorAdapter
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import de.fraunhofer.fokus.kiezradar.DataHandler
import de.fraunhofer.fokus.kiezradar.Debugger
import de.fraunhofer.fokus.kiezradar.LocationHandler.getAddressFromGeoCoordinates
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.datamodels.EventTypeModel
import de.fraunhofer.fokus.kiezradar.datamodels.SectionModel
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler
import de.fraunhofer.fokus.kiezradar.ui.TilesHandler
import de.fraunhofer.fokus.kiezradar.ui.notifications.NotificationsFragment
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import java.lang.Integer.parseInt
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread


class FilterFragment : Fragment() {

    lateinit var filterViewModel: FilterViewModel
    private lateinit var radioButtonRadiusEverything : RadioButton
    private lateinit var radioButtonRadiusDefined : RadioButton
    private lateinit var radiusInput : EditText
    lateinit var seekBar : SeekBar
    private lateinit var radioButtonLocationCurrent : RadioButton
    private lateinit var radioButtonLocationAddress : RadioButton
    private lateinit var addressInput : EditText
    lateinit var searchView: SearchView
    private lateinit var setFiltersContainer: LinearLayout
    lateinit var suggestions: ArrayList<String>
    lateinit var cursorAdapter: SimpleCursorAdapter
    private lateinit var timePeriodInputStart: EditText
    private lateinit var timePeriodInputEnd: EditText
    private lateinit var datePickerTimePeriodStart: DatePicker
    private lateinit var datePickerTimePeriodEnd: DatePicker
    private lateinit var periodOfTimeButtonToday: Button
    private lateinit var periodOfTimeButtonWeek: Button
    private lateinit var periodOfTimeButtonMonth: Button
    private lateinit var periodOfTimeButtonYear: Button
    private lateinit var headlineEventType: TextView
    private lateinit var arrowButtonEventType: ImageButton
    private lateinit var detailsEventType: LinearLayout
    private lateinit var headlineSection: TextView
    private lateinit var arrowButtonSection: ImageButton
    private lateinit var detailsSection: LinearLayout
    private lateinit var headlineLocation: TextView
    private lateinit var arrowButtonLocation: ImageButton
    private lateinit var detailsLocation: LinearLayout
    private lateinit var mapView: MapView
    private lateinit var mapViewController: IMapController
    private lateinit var headlineRadius: TextView
    private lateinit var arrowButtonRadius: ImageButton
    private lateinit var detailsRadius: LinearLayout
    private lateinit var headlineTimePeriod: TextView
    private lateinit var arrowButtonTimePeriod: ImageButton
    private lateinit var detailsTimePeriod: ConstraintLayout
    private lateinit var geoCodedAddress: String

    @RequiresApi(Build.VERSION_CODES.O)
    private val dateNow: LocalDate = LocalDate.now()
    @RequiresApi(Build.VERSION_CODES.O)
    private val dateFormatterLong = DateTimeFormatter.ofPattern("EEEE, dd. MMMM yyyy", Locale.GERMANY)
    private val dateFormatterShort = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMANY)

    private var localRadiusTileIsSet : Boolean = false
    private var localPeriodStartTileIsSet : Boolean = false
    private var localPeriodEndTileIsSet : Boolean = false

    private lateinit var localListSetFilters: ArrayList<String>

    companion object {
        var tagCallingFragment: String = ""
        val setFiltersList: ArrayList<String> = ArrayList()
        var setAddress: GeoPoint = MainActivity.actualLocationCoordinates
        var setRadius: Int = 0
        @RequiresApi(Build.VERSION_CODES.O)
        var setPeriodStart: LocalDate = LocalDate.of(0,1, 1)
        @RequiresApi(Build.VERSION_CODES.O)
        var setPeriodEnd: LocalDate = LocalDate.of(0,1, 1)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_filter, container, false)

        filterViewModel = activity?.run {
            ViewModelProvider(this).get(FilterViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val backButton: ImageButton = root.findViewById(R.id.top_bar_layout_filter_search_button_back)
        backButton.setOnClickListener{
            FragmentHandler.loadSubFragment(
                activity?.supportFragmentManager!!,
                activity?.supportFragmentManager!!.findFragmentByTag(tagCallingFragment)!!,
                tagCallingFragment,
                resources.getString(R.string.tag_filter_fragment))
            hideKeyboard()
            thread {
                context?.let { ctx -> DataHandler(ctx, requireActivity()).getFilteredEvents() }
            }
        }

        val notificationsButton: ImageButton = root.findViewById(R.id.button_notifications)
        notificationsButton.setOnClickListener{
            FragmentHandler.loadSubFragment(
                activity?.supportFragmentManager!!,
                NotificationsFragment(),
                resources.getString(R.string.tag_notifications_fragment),
                resources.getString(R.string.tag_filter_fragment)
            )
        }

        // initialize views
        searchView = root.findViewById(R.id.top_bar_layout_filter_search_view)
        setFiltersContainer = root.findViewById(R.id.filter_fragment_event_filter_container) as LinearLayout
        headlineEventType = root.findViewById(R.id.filter_fragment_text_view_event_type)
        detailsEventType = root.findViewById(R.id.filter_fragment_details_event_type)
        arrowButtonEventType = root.findViewById(R.id.filter_fragment_arrow_button_event_type)
        headlineSection = root.findViewById(R.id.filter_fragment_text_view_section)
        detailsSection = root.findViewById(R.id.filter_fragment_details_section)
        arrowButtonSection = root.findViewById(R.id.filter_fragment_arrow_button_section)
        headlineLocation = root.findViewById(R.id.filter_fragment_text_view_location)
        detailsLocation = root.findViewById(R.id.filter_fragment_details_location)
        arrowButtonLocation = root.findViewById(R.id.filter_fragment_arrow_button_location)
        mapView = root.findViewById(R.id.filter_fragment_map_view)
        radioButtonLocationCurrent = root.findViewById(R.id.filter_fragment_radio_button_location_current)
        radioButtonLocationAddress = root.findViewById(R.id.filter_fragment_radio_button_location_address)
        addressInput = root.findViewById(R.id.filter_fragment_address_input)
        headlineRadius = root.findViewById(R.id.filter_fragment_text_view_radius)
        detailsRadius = root.findViewById(R.id.filter_fragment_details_radius)
        arrowButtonRadius = root.findViewById(R.id.filter_fragment_arrow_button_radius)
        radioButtonRadiusEverything = root.findViewById(R.id.filter_fragment_radio_button_radius_everything)
        radioButtonRadiusDefined = root.findViewById(R.id.filter_fragment_radio_button_radius_defined)
        radiusInput = root.findViewById(R.id.filter_fragment_radius_input)
        seekBar = root.findViewById(R.id.filter_fragment_radius_seekBar)
        headlineTimePeriod = root.findViewById(R.id.filter_fragment_text_view_time_period)
        detailsTimePeriod = root.findViewById(R.id.filter_fragment_details_time_period)
        arrowButtonTimePeriod = root.findViewById(R.id.filter_fragment_arrow_button_time_period)
        timePeriodInputStart = root.findViewById(R.id.filter_fragment_edit_text_period_time_start_value)
        timePeriodInputEnd = root.findViewById(R.id.filter_fragment_edit_text_period_time_end_value)
        datePickerTimePeriodStart = root.findViewById(R.id.filter_fragment_date_picker_start)
        datePickerTimePeriodEnd = root.findViewById(R.id.filter_fragment_date_picker_end)
        periodOfTimeButtonToday = root.findViewById(R.id.filter_fragment_button_time_period_today)
        periodOfTimeButtonWeek = root.findViewById(R.id.filter_fragment_button_time_period_this_week)
        periodOfTimeButtonMonth = root.findViewById(R.id.filter_fragment_button_time_period_this_month)
        periodOfTimeButtonYear = root.findViewById(R.id.filter_fragment_button_time_period_this_year)

        // set listeners to views
        headlineEventType.setOnClickListener(headlineClickListener)
        headlineSection.setOnClickListener(headlineClickListener)
        headlineLocation.setOnClickListener(headlineClickListener)
        headlineRadius.setOnClickListener(headlineClickListener)
        headlineTimePeriod.setOnClickListener(headlineClickListener)
        arrowButtonEventType.setOnClickListener(arrowButtonClickListener)
        arrowButtonSection.setOnClickListener(arrowButtonClickListener)
        arrowButtonLocation.setOnClickListener(arrowButtonClickListener)
        arrowButtonRadius.setOnClickListener(arrowButtonClickListener)
        arrowButtonTimePeriod.setOnClickListener(arrowButtonClickListener)
        radiusInput.addTextChangedListener(radiusInputTextWatcher)
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener)
        addressInput.addTextChangedListener(addressInputTextWatcher)
        addressInput.onFocusChangeListener = addressInputOnFocusListener
        radioButtonRadiusEverything.setOnClickListener(radioButtonClickListener)
        radioButtonRadiusDefined.setOnClickListener(radioButtonClickListener)
        radioButtonLocationCurrent.setOnClickListener(radioButtonClickListener)
        radioButtonLocationAddress.setOnClickListener(radioButtonClickListener)

        timePeriodInputStart.onFocusChangeListener = timePeriodInputFocusChangeListener
        timePeriodInputEnd.onFocusChangeListener = timePeriodInputFocusChangeListener

        periodOfTimeButtonToday.setOnClickListener(periodOfTimeButtonClickListener)
        periodOfTimeButtonWeek.setOnClickListener(periodOfTimeButtonClickListener)
        periodOfTimeButtonMonth.setOnClickListener(periodOfTimeButtonClickListener)
        periodOfTimeButtonYear.setOnClickListener(periodOfTimeButtonClickListener)

        datePickerTimePeriodStart.setOnDateChangedListener(datePickerOnChangedDateListener)
        datePickerTimePeriodEnd.setOnDateChangedListener(datePickerOnChangedDateListener)

        // populate eventType and section multi selection
        addEventTypeCheckBoxes(MainActivity.arrayListEventTypes)
        addSectionCheckBoxes(MainActivity.arrayListSections)

        // set autocomplete for searchView
        searchView.queryHint = getString(R.string.filter_fragment_top_bar_search_hint)
        searchView.isIconifiedByDefault = false
        val from = arrayOf(SearchManager.SUGGEST_COLUMN_TEXT_1)
        val to = intArrayOf(R.id.item_label)
        cursorAdapter = SimpleCursorAdapter(
            context,
            R.layout.fragment_filter_search_item,
            null,
            from,
            to,
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        searchView.suggestionsAdapter = cursorAdapter
        suggestions = ArrayList()
        suggestions.addAll(MainActivity.arrayListSections.map { it.title })
        suggestions.addAll(MainActivity.arrayListEventTypes.map { it.title})
        suggestions.addAll(MainActivity.arrayListDistricts.map { it.title})
        searchView.setOnQueryTextListener(searchViewOnTextListener)
        searchView.setOnSuggestionListener(searchViewOnSuggestionClickListener)

        // transfer geoCoordinates to address
        geoCodedAddress = ""
        if ((activity as MainActivity).isNetworkAvailable()) {
            Debugger.logString("FILTERFRAGMENT", "APP IS ONLINE")
            geocodeActualLocation()
        }


        // viewModelObserver
        filterViewModel.radius.observe(viewLifecycleOwner, radiusValueObserver)
        filterViewModel.showAll.observe(viewLifecycleOwner, radiusShowAllObserver)

        localListSetFilters = ArrayList()
        filterViewModel.setFilters.observe(viewLifecycleOwner, setFiltersObserver)

        filterViewModel.addressIsSelected.observe(viewLifecycleOwner, addressIsSelectedObserver )
        filterViewModel.address.observe(viewLifecycleOwner, addressObserver)

        filterViewModel.periodOfTimeSelection.observe(viewLifecycleOwner, periodOfTimeSelectionObserver)
        filterViewModel.periodOfTimeStart.observe(viewLifecycleOwner, periodOfTimeStartObserver)
        filterViewModel.periodOfTimeEnd.observe(viewLifecycleOwner, periodOfTimeEndObserver)

        // OSMDROID MapView
        val policy: StrictMode.ThreadPolicy  = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context))
        mapView.isClickable = false
        mapView.setUseDataConnection(true)
        mapView.setTileSource(TileSourceFactory.MAPNIK)
        mapViewController = mapView.controller
        mapViewController.setZoom(12.0)
        mapViewController.setCenter(MainActivity.actualLocationCoordinates)
        mapView.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)

        return root
    }

    private fun createCheckBox(title: String): Pair<CheckBox, LinearLayout.LayoutParams> {
        val checkBox = CheckBox(context)
        checkBox.id = ("checkbox_$title").hashCode()
        checkBox.text = title
        checkBox.textSize = 12F
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val margin = resources.getDimension(R.dimen.filter_fragment_margin).toInt()
        layoutParams.setMargins(margin, 0, 0, 0)

        if (filterViewModel.setFilters.value?.contains(title) == true) {
            checkBox.isChecked = true
        }
        checkBox.setOnClickListener {
            if (!localListSetFilters.contains(title)) {
                filterViewModel.addFilter(title)
                localListSetFilters.add(title)
            } else {
                setFiltersContainer.removeViewInLayout(activity?.findViewById(title.hashCode()))
                filterViewModel.removeFilter(title)
                localListSetFilters.remove(title)
            }
        }

        return Pair(checkBox, layoutParams)
    }

    private fun addEventTypeCheckBoxes(arrayListEventTypes: ArrayList<EventTypeModel>) {
        for (eventType in arrayListEventTypes) {
            val (checkbox, layoutParams) = createCheckBox(eventType.title)
            detailsEventType.addView(checkbox, layoutParams)
        }
    }

    private fun addSectionCheckBoxes(arrayListSections: ArrayList<SectionModel>) {
        for (section in arrayListSections) {
            val (checkbox, layoutParams) = createCheckBox(section.title)
            detailsSection.addView(checkbox, layoutParams)
        }
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private val searchViewOnTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return false
        }

        override fun onQueryTextChange(query: String?): Boolean {
            val cursor = MatrixCursor(
                arrayOf(
                    BaseColumns._ID,
                    SearchManager.SUGGEST_COLUMN_TEXT_1
                )
            )
            query?.let {
                suggestions.forEachIndexed { index, suggestion ->
                    if (suggestion.contains(query, true)) {
                        cursor.addRow(arrayOf(index, suggestion))
                    }
                }
            }
            cursorAdapter.changeCursor(cursor)
            return true
        }

    }

    private val searchViewOnSuggestionClickListener = object : SearchView.OnSuggestionListener {
        override fun onSuggestionSelect(position: Int): Boolean {
            return false
        }

        override fun onSuggestionClick(position: Int): Boolean {
            hideKeyboard()
            val cursor = searchView.suggestionsAdapter.getItem(position) as Cursor
            val selection =
                cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1))
            searchView.setQuery(selection, false)

            filterViewModel.addFilter(selection)

            // click exit button
            searchView.isIconified = true

            return true
        }
    }

    private val headlineClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.filter_fragment_text_view_event_type -> {
                arrowButtonEventType.performClick()
            }
            R.id.filter_fragment_text_view_section -> {
                arrowButtonSection.performClick()
            }
            R.id.filter_fragment_text_view_location -> {
                arrowButtonLocation.performClick()
            }
            R.id.filter_fragment_text_view_radius -> {
                arrowButtonRadius.performClick()
            }
            R.id.filter_fragment_text_view_time_period -> {
                arrowButtonTimePeriod.performClick()
            }

        }
    }

    private val arrowButtonClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.filter_fragment_arrow_button_event_type -> {
                when (detailsEventType.visibility) {
                    View.GONE -> {
                        detailsEventType.visibility = View.VISIBLE

                        val deg: Float = arrowButtonEventType.rotation + 90F
                        arrowButtonEventType.animate().rotation(deg)
                    }
                    View.VISIBLE -> {
                        detailsEventType.visibility = View.GONE

                        val deg: Float = arrowButtonEventType.rotation - 90F
                        arrowButtonEventType.animate().rotation(deg)
                    }
                    else -> {
                        Debugger.logWarning("FILTERFRAGMENT", "Case should not be reached")
                    }
                }
            }
            R.id.filter_fragment_arrow_button_section -> {
                when (detailsSection.visibility) {
                    View.GONE -> {
                        detailsSection.visibility = View.VISIBLE

                        val deg: Float = arrowButtonSection.rotation + 90F
                        arrowButtonSection.animate().rotation(deg)
                    }
                    View.VISIBLE -> {
                        detailsSection.visibility = View.GONE

                        val deg: Float = arrowButtonSection.rotation - 90F
                        arrowButtonSection.animate().rotation(deg)
                    }
                    else -> {
                        Debugger.logWarning("FILTERFRAGMENT", "Case should not be reached")
                    }
                }
            }
            R.id.filter_fragment_arrow_button_location -> {
                when (detailsLocation.visibility) {
                    View.GONE -> {
                        detailsLocation.visibility = View.VISIBLE

                        val deg: Float = arrowButtonLocation.rotation + 90F
                        arrowButtonLocation.animate().rotation(deg)
                    }
                    View.VISIBLE -> {
                        detailsLocation.visibility = View.GONE

                        val deg: Float = arrowButtonLocation.rotation - 90F
                        arrowButtonLocation.animate().rotation(deg)
                    }
                    else -> {
                        Debugger.logWarning("FILTERFRAGMENT", "Case should not be reached")
                    }
                }
            }
            R.id.filter_fragment_arrow_button_radius -> {
                when (detailsRadius.visibility) {
                    View.GONE -> {
                        detailsRadius.visibility = View.VISIBLE

                        val deg: Float = arrowButtonRadius.rotation + 90F
                        arrowButtonRadius.animate().rotation(deg)
                    }
                    View.VISIBLE -> {
                        detailsRadius.visibility = View.GONE

                        val deg: Float = arrowButtonRadius.rotation - 90F
                        arrowButtonRadius.animate().rotation(deg)
                    }
                    else -> {
                        Debugger.logWarning("FILTERFRAGMENT", "Case should not be reached")
                    }
                }
            }
            R.id.filter_fragment_arrow_button_time_period -> {
                when (detailsTimePeriod.visibility) {
                    View.GONE -> {
                        detailsTimePeriod.visibility = View.VISIBLE

                        val deg: Float = arrowButtonTimePeriod.rotation + 90F
                        arrowButtonTimePeriod.animate().rotation(deg)
                    }
                    View.VISIBLE -> {
                        detailsTimePeriod.visibility = View.GONE

                        val deg: Float = arrowButtonTimePeriod.rotation - 90F
                        arrowButtonTimePeriod.animate().rotation(deg)
                    }
                    else -> {
                        Debugger.logWarning("FILTERFRAGMENT", "Case should not be reached")
                    }
                }
            }
        }
    }

    private val radiusInputTextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            filterViewModel.setShowAll(false)
            try {
                seekBar.progress = parseInt(s.toString())
            } catch (exc: Exception) {
                Debugger.logWarning("FILTERFRAGMENT", "Seekbar value can not be parsed")
            }
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            filterViewModel.setShowAll(false)
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    private val seekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
            // set ViewModel variable
            filterViewModel.setShowAll(false)
            try {
                filterViewModel.setRadius(i.toString())
            } catch (exc: Exception){
                Debugger.logWarning("FILTERFRAGMENT", "FilterViewModel value can not be set")
            }
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {
            filterViewModel.setShowAll(false)

        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {

        }
    }

    private val addressInputOnFocusListener = View.OnFocusChangeListener { _, hasFocus ->
        if(hasFocus) {
            filterViewModel.setAddressIsSelected(true)
        }

    }

    private val addressInputTextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    private val radioButtonClickListener = View.OnClickListener { view ->

        when (view.id) {
            R.id.filter_fragment_radio_button_radius_everything -> {
                filterViewModel.setShowAll(true)
            }

            R.id.filter_fragment_radio_button_radius_defined -> {
                filterViewModel.setShowAll(false)
            }

            R.id.filter_fragment_radio_button_location_current -> {
                filterViewModel.setAddressIsSelected(false)
            }

            R.id.filter_fragment_radio_button_location_address -> {
                filterViewModel.setAddressIsSelected(true)
            }
        }
    }

    private val timePeriodInputFocusChangeListener = View.OnFocusChangeListener { view, hasFocus ->

        when(view.id) {
            R.id.filter_fragment_edit_text_period_time_start_value -> {
                if (hasFocus) {
                    datePickerTimePeriodStart.visibility = View.VISIBLE
                } else {
                    datePickerTimePeriodStart.visibility = View.GONE
                }
            }
            R.id.filter_fragment_edit_text_period_time_end_value -> {
                if (hasFocus) {
                    datePickerTimePeriodEnd.visibility = View.VISIBLE
                } else {
                    datePickerTimePeriodEnd.visibility = View.GONE
                }
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private val periodOfTimeButtonClickListener = View.OnClickListener { view ->

        when (view.id) {
            R.id.filter_fragment_button_time_period_today -> {
                if (filterViewModel.periodOfTimeSelection.value != "today") {
                    filterViewModel.setPeriodOfTimeSelection("today")
                    val dateNowString = dateNow.format(dateFormatterLong)
                    filterViewModel.setPeriodOfTimeStart(dateNowString)
                    filterViewModel.setPeriodOfTimeEnd(dateNowString)
                } else {
                    filterViewModel.setPeriodOfTimeSelection("none")
                }
            }

            R.id.filter_fragment_button_time_period_this_week -> {
                if (filterViewModel.periodOfTimeSelection.value != "week") {
                    filterViewModel.setPeriodOfTimeSelection("week")
                    val dateStart = dateNow.with(DayOfWeek.MONDAY)
                    filterViewModel.setPeriodOfTimeStart(dateStart.format(dateFormatterLong))

                    val dateEnd = dateNow.with(DayOfWeek.SUNDAY)
                    filterViewModel.setPeriodOfTimeEnd(dateEnd.format(dateFormatterLong))
                } else {
                    filterViewModel.setPeriodOfTimeSelection("none")
                }
            }

            R.id.filter_fragment_button_time_period_this_month -> {
                if (filterViewModel.periodOfTimeSelection.value != "month") {
                    filterViewModel.setPeriodOfTimeSelection("month")
                    val dateStart: LocalDate = LocalDate.of(LocalDate.now().year, LocalDate.now().month, 1)
                    filterViewModel.setPeriodOfTimeStart(dateStart.format(dateFormatterLong))

                    val dateEnd: LocalDate = LocalDate.of(dateNow.year, dateNow.month, dateNow.lengthOfMonth())
                    filterViewModel.setPeriodOfTimeEnd(dateEnd.format(dateFormatterLong))
                } else {
                    filterViewModel.setPeriodOfTimeSelection("none")
                }
            }

            R.id.filter_fragment_button_time_period_this_year -> {
                if (filterViewModel.periodOfTimeSelection.value != "year") {
                    filterViewModel.setPeriodOfTimeSelection("year")
                    val dateStart: LocalDate = LocalDate.of(LocalDate.now().year, 1, 1)
                    filterViewModel.setPeriodOfTimeStart(dateStart.format(dateFormatterLong))

                    val dateEnd: LocalDate = LocalDate.of(dateNow.year, 12, 31)
                    filterViewModel.setPeriodOfTimeEnd(dateEnd.format(dateFormatterLong))
                } else {
                    filterViewModel.setPeriodOfTimeSelection("none")
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private val datePickerOnChangedDateListener = DatePicker.OnDateChangedListener{
        view, year, monthOfYear, dayOfMonth ->
        when (view.id) {
            R.id.filter_fragment_date_picker_start -> {
                val date = LocalDate.of(year, monthOfYear+1, dayOfMonth)
                filterViewModel.setPeriodOfTimeStart(date.format(dateFormatterLong))

                if (filterViewModel.periodOfTimeEnd.value != LocalDate.of(0,1, 1).format(dateFormatterLong)) {
                    filterViewModel.setPeriodOfTimeSelection("start_end")
                } else {
                    filterViewModel.setPeriodOfTimeSelection("start")
                }
            }
            R.id.filter_fragment_date_picker_end -> {
                val date = LocalDate.of(year, monthOfYear+1, dayOfMonth)
                filterViewModel.setPeriodOfTimeEnd(date.format(dateFormatterLong))

                if (filterViewModel.periodOfTimeStart.value != LocalDate.of(0,1, 1).format(dateFormatterLong)) {
                    filterViewModel.setPeriodOfTimeSelection("start_end")
                } else {
                    filterViewModel.setPeriodOfTimeSelection("end")
                }
            }
        }
    }

    private val radiusValueObserver: Observer<String> = Observer { radius ->
        // set radius input (seek bar and buttons are connected and changed regarding the radius input)
        if ((radius == "") && filterViewModel.showAll.value == true) {
            radiusInput.setText(filterViewModel.radius.value)
            seekBar.progress = 0
            filterViewModel.setShowAll(true)
            setRadius = 0
            localRadiusTileIsSet = false

        } else if (filterViewModel.showAll.value == false) {
            radiusInput.setText(filterViewModel.radius.value)
            if (radius != "") {
                seekBar.progress = filterViewModel.radius.value?.toInt()!!
                setRadius = radius.toInt()

                if (!localRadiusTileIsSet) {
                    context?.let {ctx ->
                        TilesHandler.addFilterTileToContainer(
                            radius,
                            "radius",
                            "",
                            "radiusTile".hashCode(),
                            ctx,
                            requireActivity(),
                            setFiltersContainer,
                            filterViewModel
                        )
                    }
                    localRadiusTileIsSet = true
                }
                else {
                    val radiusTextView = activity?.findViewById<TextView>("filterFragmentRadiusTileTextView".hashCode())
                    radiusTextView?.text = getString(R.string.text_view_radius_tile, radius)
                }
            }
        }
    }

    private val radiusShowAllObserver: Observer<Boolean> = Observer {showAll ->
        if ((showAll == true) && filterViewModel.radius.value != "") {
            radioButtonRadiusDefined.isChecked = false
            radioButtonRadiusEverything.isChecked = true
            filterViewModel.setRadius("")
            setFiltersContainer.removeViewInLayout(activity?.findViewById("filterFragmentRadiusTileLayout".hashCode()))
        } else if ((showAll == false) && filterViewModel.radius.value == "") {
            radioButtonRadiusDefined.isChecked = true
            radioButtonRadiusEverything.isChecked = false
        } else if (showAll == false) {
            radioButtonRadiusDefined.isChecked = true
            radioButtonRadiusEverything.isChecked = false
        } else if (showAll == true) {
            radioButtonRadiusDefined.isChecked = false
            radioButtonRadiusEverything.isChecked = true
        } else {
            Debugger.logWarning("FILTERFRAGMENT", "Case should not be reached")
        }
    }


    private val setFiltersObserver: Observer<MutableList<String>> = Observer { filters ->
        for (filter in filters) {
            if (!localListSetFilters.contains(filter)) {
                // add selected filter to setFiltersList
                localListSetFilters.add(filter)
                // add tile view
                context?.let {ctx ->
                    TilesHandler.addFilterTileToContainer(
                        filter,
                        "other",
                        "",
                        filter.hashCode(),
                        ctx,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel
                    )
                }
            }
            if (!setFiltersList.contains(filter)) {
                setFiltersList.add(filter)
            }
        }
        val filtersToRemove: ArrayList<String> = ArrayList()
        for (filterLocal in localListSetFilters) {
            if (!filters.contains(filterLocal)) {
                filtersToRemove.add(filterLocal)

                // uncheck checkboxes
                if ((filterLocal in MainActivity.arrayListSections.map { it.title }) or
                    (filterLocal in MainActivity.arrayListEventTypes.map { it.title})) {
                    val checkBox = activity?.findViewById<CheckBox>(("checkbox_$filterLocal").hashCode())
                    if (checkBox != null) {
                        checkBox.isChecked = false
                    }
                }
            }
        }
        localListSetFilters.removeAll(filtersToRemove)

    }

    private val addressIsSelectedObserver: Observer<Boolean> = Observer {addressIsSelected ->
        if ((addressIsSelected == true) && addressInput.text.equals(filterViewModel.address.value)) {
            radioButtonLocationCurrent.isChecked = false
            radioButtonLocationAddress.isChecked = true
        } else if ((addressIsSelected == false)) {
            radioButtonLocationCurrent.isChecked = true
            radioButtonLocationAddress.isChecked = false
            addressInput.setText("")
            mapViewController.setCenter(MainActivity.actualLocationCoordinates)
        } else if (addressIsSelected == true) {
            radioButtonLocationCurrent.isChecked = false
            radioButtonLocationAddress.isChecked = true

            setAddress = MainActivity.actualLocationCoordinates

            if ((activity as MainActivity).isNetworkAvailable()) {
                geocodeActualLocation()
            }
            addressInput.setText(geoCodedAddress)
            filterViewModel.setAddress(geoCodedAddress)

            mapViewController.setCenter(setAddress)
            mapViewController.setZoom(12.0)
        } else {
            Debugger.logWarning("FILTERFRAGMENT", "Case should not be reached")
        }
    }

    private val addressObserver: Observer<String> = Observer {address ->
        if (filterViewModel.addressIsSelected.value == true) {
            addressInput.setText(address)
        }
    }

    private val periodOfTimeSelectionObserver: Observer<String> = Observer { periodOfTimeSelection ->
        when (periodOfTimeSelection) {
            "today" -> {
                activity?.let {
                    periodOfTimeButtonToday.background =  ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background_clicked)
                    periodOfTimeButtonWeek.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonMonth.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonYear.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                }
                timePeriodInputStart.clearFocus()
                timePeriodInputEnd.clearFocus()
            }
            "week" -> {
                activity?.let {
                    periodOfTimeButtonToday.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonWeek.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background_clicked)
                    periodOfTimeButtonMonth.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonYear.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                }
                timePeriodInputStart.clearFocus()
                timePeriodInputEnd.clearFocus()
            }
            "month" -> {
                activity?.let {
                    periodOfTimeButtonToday.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonWeek.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonMonth.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background_clicked)
                    periodOfTimeButtonYear.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                }
                timePeriodInputStart.clearFocus()
                timePeriodInputEnd.clearFocus()
            }
            "year" -> {
                activity?.let {
                    periodOfTimeButtonToday.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonWeek.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonMonth.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonYear.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background_clicked)
                }
                timePeriodInputStart.clearFocus()
                timePeriodInputEnd.clearFocus()
            }
            "none" -> {
                activity?.let {
                    periodOfTimeButtonToday.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonWeek.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonMonth.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonYear.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                }
                setPeriodStart = LocalDate.of(0,1, 1)
                setPeriodEnd = LocalDate.of(0,1, 1)
                filterViewModel.setPeriodOfTimeStart("")
                filterViewModel.setPeriodOfTimeEnd("")
                timePeriodInputStart.setText("")
                timePeriodInputEnd.setText("")
            }
            "start" -> {
                activity?.let {
                    periodOfTimeButtonToday.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonWeek.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonMonth.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonYear.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                }

                setPeriodEnd = LocalDate.of(0,1, 1)
                timePeriodInputEnd.setText("")
            }
            "end" -> {
                activity?.let {
                    periodOfTimeButtonToday.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonWeek.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonMonth.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonYear.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                }

                setPeriodStart = LocalDate.of(0,1, 1)
                timePeriodInputStart.setText("")
            }
            "start_end" -> {
                activity?.let {
                    periodOfTimeButtonToday.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonWeek.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonMonth.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                    periodOfTimeButtonYear.background = ContextCompat.getDrawable(it, R.drawable.filter_fragment_period_button_background)
                }
            }
        }
    }

    private val periodOfTimeStartObserver: Observer<String> = Observer { periodOfTimeStart ->
        if (periodOfTimeStart != "") {
            timePeriodInputStart.setText(periodOfTimeStart)
            setPeriodStart = LocalDate.parse(periodOfTimeStart, dateFormatterLong)

             if (!localPeriodStartTileIsSet or (periodOfTimeStart == LocalDate.of(0,1, 1).format(dateFormatterLong))) {
                context?.let { ctx ->
                    TilesHandler.addFilterTileToContainer(
                        periodOfTimeStart,
                        "period",
                        "start",
                        "periodStart".hashCode(),
                        ctx,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel
                    )
                }
                localPeriodStartTileIsSet = true
            }
            else {
                val periodStartTextView = activity?.findViewById<TextView>("filterFragmentPeriodStartTileTextView".hashCode())

                val date = LocalDate.parse(periodOfTimeStart, dateFormatterLong)
                val formattedDate = date.format(dateFormatterShort)
                if (periodStartTextView != null) {
                    periodStartTextView.text = getString(R.string.text_view_period_time_start_tile, formattedDate)
                }
            }
        }
        else {
            setFiltersContainer.removeViewInLayout(activity?.findViewById("filterFragmentPeriodStartTileLayout".hashCode()))
            localPeriodStartTileIsSet = false
        }
    }

    private val periodOfTimeEndObserver: Observer<String> = Observer { periodOfTimeEnd ->
        if (periodOfTimeEnd != "") {
            timePeriodInputEnd.setText(periodOfTimeEnd)
            setPeriodEnd = LocalDate.parse(periodOfTimeEnd, dateFormatterLong)

            if (!localPeriodEndTileIsSet or (periodOfTimeEnd == LocalDate.of(0,1, 1).format(dateFormatterLong))) {
                context?.let { ctx ->
                    TilesHandler.addFilterTileToContainer(
                        periodOfTimeEnd,
                        "period",
                        "end",
                        "periodEnd".hashCode(),
                        ctx,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel
                    )
                }
                localPeriodEndTileIsSet = true
            } else {
                val periodEndTextView = activity?.findViewById<TextView>("filterFragmentPeriodEndTileTextView".hashCode())

                val date = LocalDate.parse(periodOfTimeEnd, dateFormatterLong)
                val formattedDate = date.format(dateFormatterShort)
                if (periodEndTextView != null) {
                    periodEndTextView.text = getString(R.string.text_view_period_time_end_tile, formattedDate)
                }
            }
        }
        else {
            setFiltersContainer.removeViewInLayout(activity?.findViewById("filterFragmentPeriodEndTileLayout".hashCode()))
            localPeriodEndTileIsSet = false
        }
    }

    private fun geocodeActualLocation(){
        thread {
            try {
                context?.let { ctx ->
                    getAddressFromGeoCoordinates(MainActivity.actualLocationCoordinates.latitude,
                        MainActivity.actualLocationCoordinates.longitude, ctx
                    )
                }?.let { address ->
                    geoCodedAddress = address }
            } catch (e: UninitializedPropertyAccessException) {
                Debugger.logError("FILTERFRAGMENT", "GEOCODING ERROR")
                println(e.stackTrace)
            }
        }
        // initialize address
        if (geoCodedAddress != "") {
            filterViewModel.setAddress(geoCodedAddress)
        }
    }
}
