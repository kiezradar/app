package de.fraunhofer.fokus.kiezradar.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import de.fraunhofer.fokus.kiezradar.BuildConfig
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler.loadSubFragment
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment
import de.fraunhofer.fokus.kiezradar.ui.notifications.NotificationsFragment

class SettingsFragment : Fragment() {

    private lateinit var settingsViewModel: SettingsViewModel
    private lateinit var settingsTextViewVersion: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)

        (activity as MainActivity).selectFragmentInBottomNav(4)

        settingsViewModel = activity?.run {
            ViewModelProvider(this).get(SettingsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val fragmentNameTextView = root.findViewById<TextView>(R.id.top_bar_layout_text_view_head_fragment_name)
        fragmentNameTextView.text = resources.getString(R.string.title_settings_fragment)

        val filterButton: ImageButton = root.findViewById(R.id.button_filter)
        filterButton.setOnClickListener {
            loadSubFragment(activity?.supportFragmentManager!!,
                            FilterFragment(),
                            resources.getString(R.string.tag_filter_fragment),
                            resources.getString(R.string.tag_settings_fragment))
        }
        val notificationsButton: ImageButton = root.findViewById(R.id.button_notifications)
        notificationsButton.setOnClickListener {
            loadSubFragment(activity?.supportFragmentManager!!,
                            NotificationsFragment(),
                            resources.getString(R.string.tag_notifications_fragment),
                            resources.getString(R.string.tag_settings_fragment))
        }

        val versionInfo = BuildConfig.VERSION_NAME
        settingsTextViewVersion = root.findViewById(R.id.settings_fragment_text_view_version)
        settingsTextViewVersion.text = getString(R.string.settings_layout_text_view_version, versionInfo)

        return root
    }
}