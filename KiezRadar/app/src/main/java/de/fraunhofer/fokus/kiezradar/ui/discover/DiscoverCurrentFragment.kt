package de.fraunhofer.fokus.kiezradar.ui.discover

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import de.fraunhofer.fokus.kiezradar.DataHandler
import de.fraunhofer.fokus.kiezradar.EventListViewAdapter
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsHandler
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsViewModel
import kotlin.concurrent.thread

class DiscoverCurrentFragment : Fragment() {

    private lateinit var listViewDiscoverCurrent: ListView
    private lateinit var eventDetailsViewModel: EventDetailsViewModel

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_discover_current, container, false)

        eventDetailsViewModel = activity?.run {
            ViewModelProvider(this).get(EventDetailsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        // find listView and set adapter + content
        listViewDiscoverCurrent = root.findViewById(R.id.eventListViewDiscoverCurrent) as ListView
        if (MainActivity.eventListFilteredSortedByDate.isNotEmpty()) {
            val adapter = EventListViewAdapter(requireContext(), MainActivity.eventListFilteredSortedByDate)
            listViewDiscoverCurrent.adapter = adapter
        } else {
            val listViewContainer = root.findViewById<ConstraintLayout>(R.id.eventListViewDiscoverCurrentEmptyContainer)
            val textView = TextView(context)
            textView.setText(R.string.discover_list_empty)
            textView.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
            listViewContainer.addView(textView)
        }

        listViewDiscoverCurrent.setOnItemClickListener { _, _, position, _ ->
            // use companion object to get event
            val selectedEventInEventDetails = MainActivity.eventListFilteredSortedByDate[position]

            EventDetailsHandler.transferAndRequestData(
                selectedEventInEventDetails,
                eventDetailsViewModel,
                requireActivity())
        }

        val swipeRefreshLayout = root.findViewById<SwipeRefreshLayout>(R.id.swipe_refresh_discover)
        swipeRefreshLayout.setOnRefreshListener {
            thread {
                context?.let { ctx -> DataHandler(ctx, requireActivity()).getFilteredEvents() }
            }
        }

        return root
    }

}