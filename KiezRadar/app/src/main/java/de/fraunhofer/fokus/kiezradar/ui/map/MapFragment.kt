package de.fraunhofer.fokus.kiezradar.ui.map

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import de.fraunhofer.fokus.kiezradar.MainActivity
import de.fraunhofer.fokus.kiezradar.R
import de.fraunhofer.fokus.kiezradar.datamodels.EventModel
import de.fraunhofer.fokus.kiezradar.ui.FragmentHandler.loadSubFragment
import de.fraunhofer.fokus.kiezradar.ui.TilesHandler
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsFragment
import de.fraunhofer.fokus.kiezradar.ui.event_details.EventDetailsViewModel
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterFragment
import de.fraunhofer.fokus.kiezradar.ui.filter.FilterViewModel
import de.fraunhofer.fokus.kiezradar.ui.notifications.NotificationsFragment
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import kotlin.concurrent.thread


class MapFragment : Fragment() {

    private lateinit var mapViewModel: MapViewModel
    private lateinit var eventDetailsViewModel: EventDetailsViewModel
    private lateinit var mapView: MapView
    private lateinit var eventMarkerDrawableIconMeeting: Drawable
    private lateinit var eventMarkerDrawableIconParticipationProject: Drawable
    private lateinit var eventMarkerDrawableIconNeighborhoodCommunity: Drawable
    private lateinit var eventMarkerDrawableIconBuildingProject: Drawable
    private lateinit var eventMarkerDrawableIconCitizensSurvey: Drawable
    private lateinit var eventClusterMarkerDrawableIcon: Drawable
    private lateinit var filterViewModel: FilterViewModel
    private lateinit var setFiltersContainer: LinearLayout
    private lateinit var localListSetFilters: ArrayList<String>

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_map, container, false)

        (activity as MainActivity).selectFragmentInBottomNav(1)

        mapViewModel = activity?.run {
            ViewModelProvider(this).get(MapViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        eventDetailsViewModel = activity?.run {
            ViewModelProvider(this).get(EventDetailsViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        filterViewModel = activity?.run {
            ViewModelProvider(this).get(FilterViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val filterButton: ImageButton = root.findViewById(R.id.button_filter)
        filterButton.setOnClickListener {
            loadSubFragment(activity?.supportFragmentManager!!,
                            FilterFragment(),
                            resources.getString(R.string.tag_filter_fragment),
                            resources.getString(R.string.tag_map_fragment))
        }
        val notificationsButton: ImageButton = root.findViewById(R.id.button_notifications)
        notificationsButton.setOnClickListener {
            loadSubFragment(activity?.supportFragmentManager!!,
                            NotificationsFragment(),
                            resources.getString(R.string.tag_notifications_fragment),
                            resources.getString(R.string.tag_map_fragment))
        }

        setFiltersContainer = root.findViewById(R.id.top_bar_event_filter_container) as LinearLayout

        filterViewModel.radius.observe(viewLifecycleOwner, radiusObserver)
        filterViewModel.periodOfTimeStart.observe(viewLifecycleOwner, periodOfTimeStartObserver)
        filterViewModel.periodOfTimeEnd.observe(viewLifecycleOwner, periodOfTimeEndObserver)
        localListSetFilters = ArrayList()
        filterViewModel.setFilters.observe(viewLifecycleOwner, setFiltersObserver)

        // OSMDROID MapView
        val policy: StrictMode.ThreadPolicy  = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context))

        mapView = root.findViewById(R.id.mapView)
        mapView.isClickable = true
        mapView.setMultiTouchControls(true)
        mapView.setUseDataConnection(true)
        mapView.setTileSource(TileSourceFactory.MAPNIK)

        // set view point on map
        val mapViewController = mapView.controller
        mapViewController.setZoom(mapViewModel.currentZoom.value!!)
        mapViewController.setCenter(mapViewModel.currentMapCenter.value)

        // fab for localization
        val fabLocalize: FloatingActionButton = root.findViewById(R.id.map_fragment_fab_localize)
        fabLocalize.setOnClickListener { view ->
            Snackbar.make(view, "Update Standort ... ", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()

            val myLocationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(context), mapView)
            mapView.overlays.add(myLocationOverlay)
            myLocationOverlay.enableMyLocation()
            myLocationOverlay.enableFollowLocation()
        }

        eventMarkerDrawableIconParticipationProject  = ContextCompat.getDrawable(requireContext(), R.drawable.map_marker_event_participation_project_foreground)!!
        eventMarkerDrawableIconParticipationProject.setBounds(0,0, eventMarkerDrawableIconParticipationProject.intrinsicWidth, eventMarkerDrawableIconParticipationProject.intrinsicHeight)
        eventMarkerDrawableIconMeeting = ContextCompat.getDrawable(requireContext(), R.drawable.map_marker_event_meeting_foreground)!!
        eventMarkerDrawableIconMeeting.setBounds(0,0, eventMarkerDrawableIconMeeting.intrinsicWidth, eventMarkerDrawableIconMeeting.intrinsicHeight)
        eventMarkerDrawableIconNeighborhoodCommunity = ContextCompat.getDrawable(requireContext(), R.drawable.map_marker_event_neighborhood_community_foreground)!!
        eventMarkerDrawableIconNeighborhoodCommunity.setBounds(0,0, eventMarkerDrawableIconNeighborhoodCommunity.intrinsicWidth, eventMarkerDrawableIconNeighborhoodCommunity.intrinsicHeight)
        eventMarkerDrawableIconBuildingProject = ContextCompat.getDrawable(requireContext(), R.drawable.map_marker_event_building_project_foreground)!!
        eventMarkerDrawableIconBuildingProject.setBounds(0,0, eventMarkerDrawableIconBuildingProject.intrinsicWidth, eventMarkerDrawableIconBuildingProject.intrinsicHeight)
        eventMarkerDrawableIconCitizensSurvey = ContextCompat.getDrawable(requireContext(), R.drawable.map_marker_event_citizens_survey_foreground)!!
        eventMarkerDrawableIconCitizensSurvey.setBounds(0,0, eventMarkerDrawableIconCitizensSurvey.intrinsicWidth, eventMarkerDrawableIconCitizensSurvey.intrinsicHeight)
        eventClusterMarkerDrawableIcon = ContextCompat.getDrawable(requireContext(), R.drawable.map_marker_event_cluster_foreground)!!
        eventClusterMarkerDrawableIcon.setBounds(0,0, eventClusterMarkerDrawableIcon.intrinsicWidth, eventClusterMarkerDrawableIcon.intrinsicHeight)

        // add marker
        thread {
            setMarkersOnMap(MainActivity.eventListFilteredSortedByDate, mapView)
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        mapView.onResume() //needed for compass, my location overlays, v6.0.0 and up
    }

    override fun onPause() {
        super.onPause()
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        mapView.onPause()  //needed for compass, my location overlays, v6.0.0 and up
    }

    override fun onDestroyView() {
        super.onDestroyView()

        mapViewModel.setCurrentMapCenter(mapView.mapCenter as GeoPoint)
        mapViewModel.setCurrentZoom(mapView.zoomLevelDouble)
    }

    private fun setMarkersOnMap(eventList: ArrayList<EventModel>, mapView: MapView) {
        val eventCluster = RadiusMarkerClusterer(requireContext())
        val eventClusterMarkerIcon = eventClusterMarkerDrawableIcon.toBitmap()
        eventCluster.setIcon(eventClusterMarkerIcon)
        eventCluster.setRadius(resources.getDimension(R.dimen.map_fragment_cluster_radius).toInt())

        mapView.overlays.add(eventCluster)

        for (event in eventList) {
            if (event.locationType == "Point") {
                val eventMarker = Marker(mapView)
                eventMarker.position = event.location[0] as GeoPoint
                eventMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
                eventMarker.subDescription = event.title

                eventMarker.setOnMarkerClickListener { _: Marker, _: MapView ->
                    // use viewModel to transfer data
                    eventDetailsViewModel.setUuid(event.uuid)
                    eventDetailsViewModel.setTitle(event.title)
                    eventDetailsViewModel.setImageUri(event.imageUrl)
                    eventDetailsViewModel.setDescription(event.description)

                    loadSubFragment(activity?.supportFragmentManager!!,
                                    EventDetailsFragment(),
                                    resources.getString(R.string.tag_event_details_fragment),
                                    resources.getString(R.string.tag_map_fragment))
                    true
                }

                when (event.eventType) {
                    getString(R.string.event_type_participation_project) -> {
                        eventMarker.icon = eventMarkerDrawableIconParticipationProject
                    }
                    getString(R.string.event_type_meeting) -> {
                        eventMarker.icon = eventMarkerDrawableIconMeeting
                    }
                    getString(R.string.event_type_neighborhood_community) -> {
                        eventMarker.icon = eventMarkerDrawableIconNeighborhoodCommunity
                    }
                    getString(R.string.event_type_citizens_survey) -> {
                        eventMarker.icon = eventMarkerDrawableIconCitizensSurvey
                    }
                    getString(R.string.event_type_building_project) -> {
                        eventMarker.icon = eventMarkerDrawableIconBuildingProject
                    }
                }
                eventCluster.add(eventMarker)
            }
        }
        mapView.invalidate()
    }

    private val radiusObserver: Observer<String> = Observer { radius ->
        if ((radius != "") && !filterViewModel.showAll.value!!) {
            context?.let { context ->
                TilesHandler.addFilterTileToContainer(
                    radius,
                    "radius",
                    "" ,
                    radius.hashCode(),
                    context,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val periodOfTimeStartObserver: Observer<String> = Observer { periodStart ->
        if (periodStart != "") {
            context?.let {
                TilesHandler.addFilterTileToContainer(periodStart,
                    "period",
                    "start",
                    periodStart.hashCode(),
                    it,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val periodOfTimeEndObserver: Observer<String> = Observer { periodEnd ->
        if (periodEnd != "") {
            context?.let {
                TilesHandler.addFilterTileToContainer(periodEnd,
                    "period",
                    "end",
                    periodEnd.hashCode(),
                    it,
                    requireActivity(),
                    setFiltersContainer,
                    filterViewModel
                )
            }
        }
    }

    private val setFiltersObserver: Observer<MutableList<String>> = Observer { filters ->
        for (filter in filters) {
            if (!localListSetFilters.contains(filter)) {
                // add selected filter to setFiltersList
                localListSetFilters.add(filter)
                // add view
                context?.let { context ->
                    TilesHandler.addFilterTileToContainer(
                        filter,
                        "other",
                        "",
                        filter.hashCode(),
                        context,
                        requireActivity(),
                        setFiltersContainer,
                        filterViewModel)
                }
            }
        }
        val filtersToRemove: ArrayList<String> = ArrayList()
        for (filterLocal in localListSetFilters) {
            if (!filters.contains(filterLocal)) {
                filtersToRemove.add(filterLocal)
            }
        }
        localListSetFilters.removeAll(filtersToRemove)
    }
}