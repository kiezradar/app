# Technische Details

## Systemvoraussetzungen

- mindestens Android 10.0

## Berechtigungen

Die KiezRadar-App benötigt bei der Installation/Nutzung folgende Rechte:

Standort
: Suche nach Ereignisse um den aktuellen Standort

Internet
: Laden der KiezRadar-Daten

Speicher
: Speichern der Einstellungen der KiezRadar-App
