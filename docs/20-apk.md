# Installation der App

Derzeit wird die KiezRadar-App nicht über einen App-Store angeboten, sie kann jedoch direkt als APK installiert werden.

Ein [Android Package (APK)](https://en.wikipedia.org/wiki/Android_application_package) ist das Dateiformat, in dem Android-Apps ausgeliefert werden.
Derzeit ist die KiezRadar-App nicht über App-Stores verfügbar, daher ist die einzige Möglichkeit, die App auszuprobieren, sie direkt auf dem Handy zu installieren.

Bitte beachten Sie: natürlich haben wir die App getestet und auf Fehler geprüft, aber die App ist derzeit in der Alpha-Phase, das heißt, es gibt noch Fehler, die wir sammeln und (hoffentlich) nach und nach ausmerzen.

Die Liste der bekannten Fehler und die Möglichkeit, eigene Probleme oder Wünsche zu erfassen finden Sie unter [Issues](https://gitlab.com/kiezradar/app/-/issues)

## Download

Die verfügbaren APKs finden Sie zum Download bei den jeweiligen Release unter [Releases](https://gitlab.com/kiezradar/app/-/releases).

## Installation

Erst einmal eine Warnung: eine APK wird direkt auf dem Handy installiert ohne Absicherung durch einen Appstore.
Bitte gehen Sie diesen Schritt nur, wenn Sie sich des möglichen Risikos bewusst sind.
Die KiezRadar-App ist im alpha-Status, das heißt, noch nicht vollständig getestet und fehlerfrei.

Auf unserem Testhandy sind bis jetzt noch keine größeren Probleme aufgetreten.

Sie benötigen mindestens Android 10.0 auf Ihrem Handy.

Wenn Sie sich dazu entschieden haben, die KiezRadar-App als APK zu installieren, gehen Sie wie folgt vor:

1. Sicherheitseinstellungen anpassen
	1. gehen Sie zu "Einstellungen" → "Sicherheit" (manchmal auch "Bildschirmsperre und Sicherheit", evtl. unter "Allgemein")
	2. dort aktivieren Sie "Unbekannte Herkunft" (Installation aus anderen Quellen zulassen)
	3. bestätigen Sie die Sicherheitswarnung
2. APK auf das Handy laden
	1. direkt aus [Releases](https://gitlab.com/kiezradar/app/-/releases) als Download
	2. oder per USB-Kabel vom Rechner
3. falls Ihr Browser die Datei umbenannt hat, so dass sie nicht auf `.apk` endet, sondern z.B. auf `.zip`, benennen Sie die Datei bitte um, so dass sie auf `.apk` endet
4. KiezRadar-App installieren
	1. heruntergeladene Datei antippen
	2. Installation bestätigen
5. Sicherheitseinstellungen wieder zurückstellen
	1. gehen Sie zu "Einstellungen" → "Sicherheit" (manchmal auch "Bildschirmsperre und Sicherheit", evtl. unter "Allgemein")
	2. dort deaktivieren Sie "Unbekannte Herkunft" (Installation aus anderen Quellen zulassen)
