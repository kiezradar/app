# Einführung

![Logo - ein stilisiertes Radar mit KiezRadar-Schriftzug](images/logo.png){ align=right }

Was passiert in meinem Kiez?
Welche Beteiligungsmöglichkeiten gibt es in meiner Umgebung?
Womit befasst sich die Lokalpolitik?

Statt sich selbst durch die digitalen Angebote Berlins zu wühlen, soll die KiezRadar-App Bürger:innen zukünftig proaktiv und bedarfsgerecht über wichtige Ereignisse aus Politik und Verwaltung informieren.
Beispielsweise sollen die Nutzer:innen die Themengebiete oder den Umkreis für den ihnen Beteiligungen angezeigt werden selbst wählen können.
Dabei sollen die Informationen der bisherigen Informationsangebote wie bspw. mein.Berlin oder das Ratsinformationssystem genutzt und gezielt für die Nutzer:innen aufbereitet werden.

Fehler, Wünsche
: https://gitlab.com/kiezradar/app/-/issues

Downloads/Releases
: https://gitlab.com/kiezradar/app/-/releases

Homepage
: https://kiezradar.fokus.fraunhofer.de
